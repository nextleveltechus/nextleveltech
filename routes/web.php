<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('auth/login/auto', 'Auth\LoginController@auto')
    ->name('auth.login.auto');
    Route::get('auth/login/forgot_username', function () {
        return view('auth.login.forgot_username');
    })->name('auth.login.forgot_username');
    Route::post('auth/login/forgot_username_success', 'Auth\LoginController@forgot_username_success')
        ->name('auth.login.forgot_username_success');
    Route::get('auth/login/lost_password', function () {
        return view('auth.login.lost_password');
    })->name('auth.login.lost_password');
    Route::post('auth/login/lost_password_success', 'Auth\LoginController@lost_password_success')
        ->name('auth.login.lost_password_success');
    Route::get('auth/login/token_login/{slashData?}', 'Auth\LoginController@token_login')
        ->where('slashData', '(.*)')
        ->name('auth.login.token_login');

Route::match(['get','post'],'/api/paypal/webhook', 'Api\PayPalController@webhook');

Route::get('api/files/download/{slashData?}', 'Api\FilesController@download')
    ->where('slashData', '(.*)')
    ->name('api.files.download');
    Route::get('api/files/show/{slashData?}', 'Api\FilesController@show')
        ->where('slashData', '(.*)')
        ->name('api.files.show');
    Route::get('api/files/license', 'Api\FilesController@license')
        ->name('api.files.license');
    Route::resource('/api/files', 'Api\FilesController')
        ->names([
            'show' => 'api.files.show'
        ]);

Route::get('account/uploads/index/{slashData?}', 'Account\UploadsController@index')
    ->where('slashData', '(.*)')
    ->name('account.uploads.index');
    Route::post('account/uploads/searchQuick/{slashData?}', 'Account\UploadsController@searchQuick')
        ->where('slashData', '(.*)')
        ->name('account.uploads.searchQuick');
    Route::post('account/uploads/saveShowOnly/{slashData?}', 'Account\UploadsController@saveShowOnly')
        ->where('slashData', '(.*)')
        ->name('account.uploads.saveShowOnly');
    Route::post('account/uploads/saveNumPerPage/{slashData?}', 'Account\UploadsController@saveNumPerPage')
        ->where('slashData', '(.*)')
        ->name('account.uploads.saveNumPerPage');
    Route::post('account/uploads/store/{slashData?}', 'Account\UploadsController@store')
        ->where('slashData', '(.*)')
        ->name('account.uploads.store');
    Route::post('account/uploads/destroy/{slashData?}', 'Account\UploadsController@destroy')
        ->where('slashData', '(.*)')
        ->name('account.uploads.destroy');

Route::get('/', 'IndexController@index')
    ->name('index');

Route::get('/api/alerts/toggleHelpBlocks', 'Api\AlertsController@toggleHelpBlocks');
Route::get('/api/common/returnCity', 'Api\CommonController@returnCity');
Route::get('/api/common/returnState', 'Api\CommonController@returnState');
Route::get('/api/common/returnCounty', 'Api\CommonController@returnCounty');

Route::view('admin', 'admin.index.index', ['name' => 'admin']);

Route::get('admin/users/index/{slashData?}', 'Admin\UsersController@index')
    ->where('slashData', '(.*)')
    ->name('admin.users');
    Route::post('admin/users/searchQuick', 'Admin\UsersController@searchQuick')
        ->name('admin.users.searchQuick');
    Route::post('admin/users/saveNumPerPage', 'Admin\UsersController@saveNumPerPage')
        ->name('admin.users.saveNumPerPage');
    Route::post('admin/users/showOnly', 'Admin\UsersController@showOnly')
        ->name('admin.users.showOnly');
    Route::match(['get','post'],'admin/users/add', 'Admin\UsersController@add')
        ->name('admin.users.add');
    Route::match(['get','post'],'admin/users/edit/{slashData?}', 'Admin\UsersController@edit')
        ->where('slashData', '(.*)')
        ->name('admin.users.edit');

Route::get('admin/emails/index/{slashData?}', 'Admin\EmailsController@index')
    ->where('slashData', '(.*)')
    ->name('admin.emails');
    Route::get('admin/emails/show/{id}', 'Admin\EmailsController@show')
        ->name('admin.emails.show');
    Route::post('admin/emails/searchQuick', 'Admin\EmailsController@searchQuick')
        ->name('admin.emails.searchQuick');
    Route::post('admin/emails/saveNumPerPage', 'Admin\EmailsController@saveNumPerPage')
        ->name('admin.emails.saveNumPerPage');
    Route::post('admin/emails/destroy', 'Admin\EmailsController@destroy')
        ->name('admin.emails.destroy');

Route::get('admin/tickets/index/{slashData?}', 'Admin\TicketsController@index')
    ->where('slashData', '(.*)')
    ->name('admin.tickets.index');
    Route::post('admin/tickets/searchQuick', 'Admin\TicketsController@searchQuick')
        ->name('admin.tickets.searchQuick');
    Route::post('admin/tickets/saveShowOnly', 'Admin\TicketsController@saveShowOnly')
        ->name('admin.tickets.saveShowOnly');
    Route::post('admin/tickets/saveShowOnlyAdmin', 'Admin\TicketsController@saveShowOnlyAdmin')
        ->name('admin.tickets.saveShowOnlyAdmin');
    Route::post('admin/tickets/saveNumPerPage', 'Admin\TicketsController@saveNumPerPage')
        ->name('admin.tickets.saveNumPerPage');
    Route::resource('admin/tickets', 'Admin\TicketsController')
        ->names([
            'create' => 'admin.tickets.create'
            ,'store' => 'admin.tickets.store'
            ,'edit' => 'admin.tickets.edit'
            ,'update' => 'admin.tickets.update'
        ]);

Route::get('admin/contacts/index/{slashData?}', 'Admin\ContactsController@index')
    ->where('slashData', '(.*)')
    ->name('admin.contacts.index');
    Route::post('admin/contacts/searchQuick', 'Admin\ContactsController@searchQuick')
        ->name('admin.contacts.searchQuick');
    Route::post('admin/contacts/saveShowOnly', 'Admin\ContactsController@saveShowOnly')
        ->name('admin.contacts.saveShowOnly');
    Route::post('admin/contacts/saveShowOnlyAdmin', 'Admin\ContactsController@saveShowOnlyAdmin')
        ->name('admin.contacts.saveShowOnlyAdmin');
    Route::post('admin/contacts/saveNumPerPage', 'Admin\ContactsController@saveNumPerPage')
        ->name('admin.contacts.saveNumPerPage');
    Route::resource('admin/contacts', 'Admin\ContactsController')
        ->names([
            'create' => 'admin.contacts.create'
            ,'store' => 'admin.contacts.store'
            ,'edit' => 'admin.contacts.edit'
            ,'update' => 'admin.contacts.update'
            ,'destroy' => 'admin.contacts.destroy'
        ]);

Route::resource('admin/ticketresponses', 'Admin\TicketResponsesController')
    ->names([
        'update' => 'admin.ticketresponses.update'
        ,'destroy' => 'admin.ticketresponses.destroy'
    ]);

Route::get('/info', 'InfoController@index')->name('info.index');
Route::get('/info/terms', 'InfoController@terms')->name('info.terms');
Route::get('/info/privacy', 'InfoController@privacy')->name('info.privacy');

Route::get('info/server-management', function () {
    return view('info.server-management');
})->name('info.server-management');
Route::get('info/information-technology-staffing', function () {
    return view('info.information-technology-staffing');
})->name('info.information-technology-staffing');
Route::get('info/phone-apps', function () {
    return view('info.phone-apps');
})->name('info.phone-apps');

Route::get('/support', 'Support\IndexController@index')->name('support.index');
Route::get('/support/contact/index', 'Support\ContactController@index')->name('support.contact.index');
Route::post('/support/contact/create', 'Support\ContactController@create')->name('support.contact.create');
Route::get('/support/contact/success', 'Support\ContactController@success')->name('support.contact.success');

Route::get('support/tickets/index/{slashData?}', 'Support\TicketsController@index')
    ->where('slashData', '(.*)')
    ->name('support.tickets.index');
    Route::post('support/tickets/searchQuick', 'Support\TicketsController@searchQuick')
        ->name('support.tickets.searchQuick');
    Route::post('support/tickets/saveShowOnly', 'Support\TicketsController@saveShowOnly')
        ->name('support.tickets.saveShowOnly');
    Route::post('support/tickets/saveNumPerPage', 'Support\TicketsController@saveNumPerPage')
        ->name('support.tickets.saveNumPerPage');
    Route::resource('support/tickets', 'Support\TicketsController')
        ->names([
            'create' => 'support.tickets.create'
            ,'store' => 'support.tickets.store'
            ,'edit' => 'support.tickets.edit'
            ,'update' => 'support.tickets.update'
        ]);

Route::resource('support/ticketresponses', 'Support\TicketResponsesController')
    ->names([
        'update' => 'support.ticketresponses.update'
        ,'destroy' => 'support.ticketresponses.destroy'
    ]);

Route::resource('contact', 'ContactController')
    ->names([
        'index' => 'contact'
        ,'create' => 'contact.create'
        ,'update' => 'contact.update'
    ]);


Route::match(['get','post'],'/account', 'Account\IndexController@index')
    ->name('account');
