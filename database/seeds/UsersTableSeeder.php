<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use \App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('users')->insert([
            'username' => 'kris'
            ,'password' => bcrypt('smith')
            ,'fname' => 'Kris'
            ,'lname' => 'Smith'
            ,'email' => 'support@nextleveltech.us'
            ,'timezone' => User::TIMEZONE['Eastern']
            ,'type' => strtoupper(User::TYPE['SYSTEM ADMINISTRATOR'])
            ,'status' => strtoupper(User::STATUS['ACTIVE'])
            ,'created_at' => date("Y-m-d H:i:s")
            ,'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
