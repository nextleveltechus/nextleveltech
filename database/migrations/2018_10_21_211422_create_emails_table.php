<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');

            $table->string('to',1000);
            $table->string('from',100);
            $table->string('name',100);
            $table->string('phone',100);
            $table->text('message');
            $table->text('auth')->nullable();
            $table->text('request')->nullable();
            $table->text('session')->nullable();
            $table->text('cookie')->nullable();
            $table->text('server')->nullable();
            $table->string('deleted',5)->default('FALSE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
