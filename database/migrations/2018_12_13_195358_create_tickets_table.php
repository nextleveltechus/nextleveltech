<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title',100)->index();
            $table->text('description');
            $table->string('type',100);
            $table->string('status',100);
            $table->string('priority',100);
            $table->integer('startDate')->nullable();
            $table->integer('endDate')->nullable();
            $table->integer('estimatedTime')->nullable();
            $table->integer('percentComplete')->default(0)->index();
            $table->integer('userId');
            $table->integer('assigneeId');

            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
