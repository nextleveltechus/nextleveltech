<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('username',100)->index();
            $table->string('password',100);
            $table->string('fname',100)->index();
            $table->string('lname',100)->index();
            $table->string('email',100)->index();
            $table->string('timezone');
            $table->string('type',100);
            $table->string('status',100);
            $table->integer('password_token_timeout')->default(0);
            $table->string('password_token',100)->nullable();

            $table->timestamps();

            $table->unique('username');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
