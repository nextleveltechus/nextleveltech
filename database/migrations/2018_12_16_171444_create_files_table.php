<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');

            $table->string('basepath',1000);
            $table->string('title',100);
            $table->string('raw_name',100);
            $table->string('file_ext',100);
            $table->integer('file_size');
            $table->integer('image_width')->nullable();
            $table->integer('image_height')->nullable();
            $table->string('image_size_str',100)->nullable();
            $table->string('mime',100)->nullable();
            $table->integer('userId');
            $table->string('instanceType',100);
            $table->integer('instanceId');

            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
