@extends('common.layout')

@section('content')
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script type="text/javascript">
		function onSubmit(token) {
			document.getElementById("google_captcha_form").submit();
		}
	</script>

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			{{ Form::open() }}
				@csrf
				<div class="form-group">
					{{ Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => 'username']) }}
				</div>
	            <div class="form-group">
					{{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'password']) }}
				</div>
				<div>
					<a href="{{ route('auth.login.forgot_username') }}" class="pull-right">{{ __('Forgot your username?') }}</a>
					<br />
					<a href="{{ route('auth.login.lost_password') }}" class="pull-right">{{ __('Lost your password?') }}</a>
				</div>
				<br /><br />
				<div>
	            	{{ Form::submit('Login',['class' => 'btn btn-primary pull-right']) }}
				</div>
	        {{ Form::close() }}
	    </div>

		<?/*
		<div class="col-xs-12 col-sm-6">
			{{ Form::open(array('url' => 'register', 'id' => 'google_captcha_form')) }}
				@csrf
				<h4>Registration</h4>
				<div class="form-group">
					{{ Form::label('username', 'Username') }}
					{{ Form::text('username', old('username'), ['class' => 'form-control']) }}
					<p class="help-block-force">
						The username you will use to login.<br />
						(Limit 6-25 alphanumeric characters)
					</p>
				</div>
				<div class="form-group">
					{{ Form::label('password', 'Password') }}
					{{ Form::password('password', ['class' => 'form-control']) }}
					<p class="help-block-force">
						The password you will use to login.<br />
						(Limit 6-25 characters, Must contain a capital letter, a lowercase letter, a number, and a special character)
					</p>
				</div>
				<div class="form-group">
					{{ Form::label('confirm', 'Confirm Password') }}
					{{ Form::password('confirm', ['class' => 'form-control']) }}
					<p class="help-block-force">
						Retype your chosen password.<br />
						(Limit 25 alphanumeric characters)
					</p>
				</div>
				<div class="form-group">
					{{ Form::label('email', 'Email') }}
					{{ Form::text('email', old('email'), ['class' => 'form-control']) }}
					<p class="help-block-force">
						Your email address. This will be used for password recovery.<br />
						(Valid email address, Limit 100 characters)
					</p>
				</div>
				<div class="form-group">
					{{ Form::label('timezone', 'Time Zone') }}
					{{ Form::select('timezone', array_flip(\App\User::TIMEZONE), old('timezone'),
					['class' => 'form-control']) }}
					<p class="help-block-force">
						The time zone that you operate in.  The times on this website
						will change depending on your time zone.
					</p>
				</div>

				<br />
				<h4>Personal Information</h4>
				<div class="form-group">
					{{ Form::label('fname', 'First Name') }}
					{{ Form::text('fname', old('fname'), ['class' => 'form-control']) }}
					<p class="help-block-force">
						Your first name.<br />
						(Limit 100 characters)
					</p>
				</div>
				<div class="form-group">
					{{ Form::label('lname', 'Last Name') }}
					{{ Form::text('lname', old('lname'), ['class' => 'form-control']) }}
					<p class="help-block-force">
						Your last name.<br />
						(Limit 100 characters)
					</p>
				</div>
				<div class="form-group">
					{{ Form::checkbox('terms and conditions', 'on', old('terms and conditions')) }}
					I accept the terms and conditions of this website.
				</div>

				<button class="g-recaptcha btn btn-default btn-default"
					data-sitekey="{{ config('app.google_recaptcha_site_key') }}"
					data-callback='onSubmit'
					data-badge="bottomleft">Register</button>
			{{ Form::close() }}
		</div>
		*/?>
	</div>
@endsection
