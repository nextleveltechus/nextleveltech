<div class="col-xs-12 col-sm-6">
	<div class="text-center">
		<a class="trigger"><h1>{{ $type }} Registration</h1></a>
	</div>

	<div class="none">
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<script type="text/javascript">
			function onSubmit_{{ $type }}(token) {
				document.getElementById("google_captcha_form_{{ $type }}").submit();
			}
		</script>

		{{ Form::open(array('url' => route('register'), 'id' => "google_captcha_form_{$type}", 'class' => 'registration')) }}
			@csrf
			<div class="form-group">
				{{ Form::label('username', 'Username') }}
				{{ Form::text('username', old('username'), ['class' => 'form-control']) }}
				<p class="help-block-force">
					The username you will use to login.<br />
					(Limit 6-25 alphanumeric characters)
				</p>
			</div>
			<div class="form-group">
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password', ['class' => 'form-control']) }}
				<p class="help-block-force">
					The password you will use to login.<br />
					(Limit 6-25 characters, Must contain a capital letter, a lowercase letter, a number, and a special character)
				</p>
			</div>
			<div class="form-group">
				{{ Form::label('confirm', 'Confirm Password') }}
				{{ Form::password('confirm', ['class' => 'form-control']) }}
				<p class="help-block-force">
					Retype your chosen password.<br />
					(Limit 25 alphanumeric characters)
				</p>
			</div>
			<div class="form-group">
				{{ Form::label('email', 'Email') }}
				{{ Form::text('email', old('email'), ['class' => 'form-control']) }}
				<p class="help-block-force">
					Your email address. This will be used for password recovery.<br />
					(Valid email address, Limit 100 characters)
				</p>
			</div>
			<div class="form-group">
				{{ Form::label('timezone', 'Time Zone') }}
				{{ Form::select('timezone', array_flip(\App\User::TIMEZONE), old('timezone'),
				['class' => 'form-control']) }}
				<p class="help-block-force">
					The time zone that you operate in.  The times on this website
					will change depending on your time zone.
				</p>
			</div>

			<br />
			<h4>Personal Information</h4>
			<div class="form-group">
				{{ Form::label('fname', 'First Name') }}
				{{ Form::text('fname', old('fname'), ['class' => 'form-control']) }}
				<p class="help-block-force">
					Your first name.<br />
					(Limit 100 characters)
				</p>
			</div>
			<div class="form-group">
				{{ Form::label('lname', 'Last Name') }}
				{{ Form::text('lname', old('lname'), ['class' => 'form-control']) }}
				<p class="help-block-force">
					Your last name.<br />
					(Limit 100 characters)
				</p>
			</div>
			<div class="form-group">
				{{ Form::checkbox('terms and conditions', 'on', old('terms and conditions')) }}
				I accept the terms and conditions of this website.
			</div>

			{{ Form::hidden('type', $type) }}
			<button class="g-recaptcha btn btn-default btn-default"
				data-sitekey="{{ config('app.google_recaptcha_site_key') }}"
				data-callback='onSubmit_{{ $type }}'
				data-badge="bottomleft">Register</button>
		{{ Form::close() }}
	</div>
</div>
