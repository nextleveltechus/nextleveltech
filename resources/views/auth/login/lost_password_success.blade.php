@extends('common.layout')

@section('content')
    <div class="row short_text text-center">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <h5>Thank you.  Instructions on how you can reset your password has been
            sent to the email address associated with your account.</h5>
            <br /><br />
            <a href="{{ route('index') }}">home</a>
        </div>
    </div>
@endsection
