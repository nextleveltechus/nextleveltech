On <?=date("l, F j, Y")?> at <?=date("g:ia T")?> a username recovery request was created.
<br /><br />
The usernames associated with this email address are:
<br />
<? foreach($users as $k => $v): ?>
    <div>{{ $v->username }} - {{ \App\User::STATUS[strtoupper($v->status)] }}</div>
<? endforeach; ?>
