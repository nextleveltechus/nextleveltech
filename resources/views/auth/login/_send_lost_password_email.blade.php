On {{ date("l, F j, Y") }} at {{ date("g:ia T") }} a password recovery request was created.
<br /><br />
Click the link below to being the password recovery process.<br />Your password recovery
token will expire on {{ date("l, F j, Y", $user->password_token_timeout) }} at {{ date("g:ia T", $user->password_token_timeout) }}.
<br /><br />
<a href="{{ displayProtocol() }}{{ $_SERVER['HTTP_HOST'] }}/auth/login/token_login/username/{{ $user->username }}/token/{{ $user->password_token }}">
    {{ displayProtocol() }}{{ $_SERVER['HTTP_HOST'] }}/auth/login/token_login/username/{{ $user->username }}/token/{{ $user->password_token }}
</a>
