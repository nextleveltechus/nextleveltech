@extends('common.layout')

@section('content')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        function onSubmit(token) {
            document.getElementById("google_captcha_form").submit();
        }
    </script>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            {{ Form::open(array('url' => 'auth/login/lost_password_success', 'id' => 'google_captcha_form')) }}
                @csrf
                <h4>Lost Password</h4>
                <div class="form-group">
                    {{ Form::label('username', 'Username') }}
                    {{ Form::text('username', old('username'), ['class' => 'form-control', 'id' => 'username']) }}
                    <p class="help-block">
						Please enter the username you created when you registered.
					</p>
                </div>
				<div>
	            	{{ Form::submit('Submit',['class' => 'btn btn-default']) }}
				</div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
