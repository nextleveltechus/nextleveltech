@extends('common.layout')

@section('content')
    <div class="row short_text text-center">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <h5>Thank you.  If a username is found that is associated with that email address,
            it will we sent to the email address you provided.</h5>
            <br /><br />
            <a href="{{ route('index') }}">home</a>
        </div>
    </div>
@endsection
