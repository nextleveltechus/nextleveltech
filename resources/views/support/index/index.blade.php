@extends('common/layout')

@section('content')
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('support.contact.index') }}" class="btn btn-default btn-block">Contact</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('support.tickets.index') }}" class="btn btn-default btn-block">Tickets</a>
        </div>
    </div>

    <br /><br /><br />
@endsection
