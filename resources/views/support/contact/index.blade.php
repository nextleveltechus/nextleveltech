@extends('common/layout')

@section('content')
    <div class="help-block col-sm-10 col-sm-offset-1">
        <h4 class="page-title">Contact</h4>
        <div>This is a contact form.  You can use this form to send an email to {{ config('app.name') }}.
        Please allow 24-48 hours for a response.</div>
        <br /><br /><br />
    </div>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
		function onSubmit(token) {
			document.getElementById("google_captcha_form").submit();
		}
	</script>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <form action="{{ route('support.contact.create') }}" method="post" id="google_captcha_form">
                @csrf
                <div class="form-group">
                    <label for="name">Your Name</label>
                    <input type="text" name="name" value="<?= old('name')??@Auth::user()->fname . ' ' . @Auth::user()->lname ?>" class="form-control" />
                </div>

                <div class="form-group">
                    <label for="email">Your Email Address</label>
                    <input type="text" name="email" value="<?= old('email')??@Auth::user()->email ?>" class="form-control" />
                </div>

                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea name="message" class="form-control" rows="10">{{ old('message') }}</textarea>
                </div>

                <button class="g-recaptcha btn btn-default pull-right"
					data-sitekey="{{ config('app.google_recaptcha_site_key') }}"
					data-callback='onSubmit'
                    data-badge="bottomleft">Send Email</button>
                <br /><br /><br /><br />
            </form>
        </div>
    </div>
@endsection
