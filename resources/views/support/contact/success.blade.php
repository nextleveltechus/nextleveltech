@extends('common/layout')

@section('content')
    <div class="row short_text text-center">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <h5>Thank you.  Your email has been sent.
                <br /><br />Please allow 48 hours for a response.</h5>
                <br /><br /><a href="{{ route('index') }}">home</a>
        </div>
    </div>
@endsection
