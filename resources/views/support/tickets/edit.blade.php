@extends('common/layout')

@section('content')
    <h3 class="text-center">Details for Ticket #{{ $ticket->id }}</h3>
    <div class="row">
    	<div class="col-xs-12">
    		<a href="" class="btn btn-default" id="updateFormTrigger">Toggle Update</a>
    	</div>
    </div>
    <br />
    <div class="well">
        <div class="row">
    		<div class="col-xs-12 text-right">
    			<h4>{{ displayFromStringDateTime($ticket->created_at) }}</h4>
    		</div>
        </div>
    	<div class="row">
    		<div class="col-xs-12">
    			<div><strong>Title</strong></div>
    			<div>{{ $ticket['title'] }}</div>
    		</div>
    	</div>
    	<br />
    	<div class="row">
    		<div class="col-xs-12">
    			<div><strong>Description</strong></div>
    			<div><pre>{{ $ticket->description }}</pre></div>
    		</div>
    	</div>

        <? if(!$files->isEmpty()): ?>
        	<div class="row">
        		<div class="col-xs-12">
        			<br />
        			<div><strong>Files</strong></div>
        			<? foreach($files as $k => $v): ?>
                        <div>
                            <? if(in_array($v->file_ext,['png','jpg','gif','jpeg'])): ?>
                                <a href="" data-toggle="modal" data-target="#img{{ $v->id }}">{{ $v['title'] }}</a>
                            <? else: ?>
                                {{ $v['title'] }}
                            <? endif; ?>
                            - <a href="{{ route('api.files.download',$v->id) }}">{{ $v->raw_name }}.{{ $v->file_ext }}</a>
            				<div class="modal fade text-center" tabindex="-1" role="dialog" id="img{{ $v->id }}">
            					<div class="modal-dialog" role="document">
            						<div class="modal-content">
            							<div class="modal-header">
            								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            							</div>
            							<div class="modal-body">
            								<img src="{{ route('api.files.show',$v->id) }}" />
            							</div>
            						</div>
            					</div>
            				</div>
                        </div>
        			<? endforeach; ?>
        		</div>
        	</div>
    	<? endif; ?>
    </div>

    {{ Form::open(['url' => route('support.tickets.update',$ticket->id), 'method' => 'put', 'files' => TRUE]) }}
        <div id="updateForm">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        {{ Form::label('response', 'New Response') }}&nbsp;&nbsp;<a href="" id="moreTrigger">More</a>
                        {{ Form::textarea('response', old('response'), ['class' => 'form-control', 'id' => 'response']) }}
                    </div>
                </div>
            </div>
            @csrf
            <div id="more">
                <div class="row">
            		<div class="col-xs-12">
                		<div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', old('title')??@$ticket['title'], ['class' => 'form-control', 'id' => 'title']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
            		<div class="col-xs-12">
                		<div class="form-group">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::textarea('description', old('description')??@$ticket->description, ['class' => 'form-control', 'id' => 'desription']) }}
                		</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <hr />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('fileTitle', 'File Title') }}
                        {{ Form::text('fileTitle', old('fileTitle')??@$ticket->fileTitle, ['class' => 'form-control', 'id' => 'fileTitle']) }}
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('userfile', 'File Upload') }}
                        {{ Form::file('userfile', ['class' => 'form-control', 'id' => 'userfile']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                        <a href="{{ route('support.tickets.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

    <? if(!empty($ticketResponses)): ?>
        <? foreach($ticketResponses as $k => $v): ?>
            <div class="well">
            	<div class="row text-right">
            		<div class="col-xs-12">
            			<h4>{{ displayFromStringDateTime($v->created_at) }}</h4>
            		</div>
            	</div>

            	{{ listArray($v['updates']) }}

            	<div class="row">
                    <div class="col-xs-12">
                        <pre>{{ $v->response }}</pre>
                    </div>
                </div>
            	<div class="row responseDescription" id="responseDescription_{{ $v->id }}"><div class="col-xs-12">
                    {{ Form::open(['url' => route('support.ticketresponses.update',$v->id), 'method' => 'put']) }}
                        @csrf
            			<br />
                        {{ Form::label('editDescription', 'Edit Response') }}
                        {{ Form::textarea('response', old('response')??@$v->response, ['class' => 'form-control', 'id' => 'response']) }}
            			<br />
                        <div class="form-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
            				<a href="" id="editResponseCancel_{{ $v->id }}" role="button" class="btn btn-default editResponseCancel">Cancel</a>
            			</div>
            		{{ Form::close() }}
            	</div></div>
            </div>
        <? endforeach; ?>
    <? endif; ?>

    <script language="javascript">
    	$(document).ready(function() {
    		$("#updateFormTrigger").click(function(e){
    			e.preventDefault();
    			$("#updateForm").animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$("#moreTrigger").click(function(e){
    			e.preventDefault();
    			$("#more").animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$(".editResponse").click(function(e){
    			e.preventDefault();
    			id = $(this).attr('id');
    			$("#responseDescription_"+id).animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$(".editResponseCancel").click(function(e){
    			e.preventDefault();
                id = $(this).attr('id').split('_');
    			$("#responseDescription_"+id[1]).animate({ opacity: 'toggle', height: 'toggle' });
    		});
    	});
    </script>
@endsection
