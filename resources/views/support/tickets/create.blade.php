@extends('common/layout')

@section('content')
    {{ Form::open(['url' => route('support.tickets.store'), 'method' => 'post', 'files' => TRUE]) }}
        @csrf
        <div class="row">
    		<div class="col-xs-12">
        		<div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', old('title')??@$ticket['title'], ['class' => 'form-control', 'id' => 'title']) }}
                </div>
            </div>
        </div>
        <div class="row">
    		<div class="col-xs-12">
        		<div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', old('description')??@$ticket->description, ['class' => 'form-control', 'id' => 'desription']) }}
        		</div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    {{ Form::label('fileTitle', 'File Title') }}
                    {{ Form::text('fileTitle', old('fileTitle')??@$ticket->fileTitle, ['class' => 'form-control', 'id' => 'fileTitle']) }}
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    {{ Form::label('userfile', 'File Upload') }}
                    {{ Form::file('userfile', ['class' => 'form-control', 'id' => 'userfile']) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                    <a href="{{ route('support.tickets.index') }}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection
