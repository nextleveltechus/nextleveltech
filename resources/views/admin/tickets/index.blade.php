@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8 search-col">
            <form action="{{ route('admin.tickets.searchQuick') }}" method="post" accept-charset="utf-8">
                <div class="input-group">
                    <input type="text" name="search" value="" class="form-control" id="search" placeholder="Search">
                    <div class="input-group-btn">
                        @csrf
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <? if(method_exists($tickets,'links')): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action="{{ route('admin.tickets.saveShowOnly') }}" method="post" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-addon">Show Only</span>
                        @csrf
                        <select name="status" class="form-control showOnly">
                            <option value="">All Open</option>
                            <? foreach(\App\Ticket::STATUS as $k => $v): ?>
                                <option value="<?=$v?>"<? if($v == $ticketsShowOnly): ?> SELECTED<? endif; ?>><?=ucwords(strtolower($v))?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action="{{ route('admin.tickets.saveShowOnlyAdmin') }}" method="post" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-addon">Show Only</span>
                        @csrf
                        <select name="assigneeId" class="form-control showOnly">
                            <option value="">My Tickets</option>
                            <option value="all"<? if($ticketsShowOnlyAdmin == 'all'): ?> SELECTED<? endif; ?>>All Tickets</option>
                            <? foreach(\App\User::system_admins() as $k => $v): ?>
                                <option value="<?=$k?>"<? if($k == $ticketsShowOnlyAdmin): ?> SELECTED<? endif; ?>><?=$v?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action='{{ route('admin.tickets.saveNumPerPage') }}' method="post">
                    <div class="input-group">
                        <span class="input-group-addon">Show</span>
                        @csrf
                        {{ Form::select('numPerPage'
                        ,\App\Common::NUMPERPAGE
                        ,$numPerPage
                        ,["class" => 'numPerPage form-control']) }}
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-sm-6 search-col pull-right right">
                <span style="float:right;">{{ $tickets->links() }}</span>
            </div>
        </div>
    <? endif; ?>

    <? if(!empty($search)): ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                Searching for <i><?=$search?></i>
            </div>
        </div>
        <br />
    <? endif; ?>

    <div class="row">
        <div class="col-xs-12 text-center">
            <a href="{{ route('admin.tickets.create') }}" class="btn btn-default">Add New Ticket</a>
        </div>
    </div>

    <br />

    <? if(!$tickets->isEmpty()): ?>
        <div class="table-responsive no-border">
            <table class="table">
                <tr class="tableStripe">
                    <th></th>
                    <th>
                        <a href="{{ route('admin.tickets.index') }}/sort/title/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Title
                        <a href="{{ route('admin.tickets.index') }}/sort/title/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>
                        Assigner
                    </th>
                    <th>
                        Assignee
                    </th>
                    <th>
                        <a href="{{ route('admin.tickets.index') }}/sort/percentComplete/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        % Done
                        <a href="{{ route('admin.tickets.index') }}/sort/percentComplete/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th>
                        <a href="{{ route('admin.tickets.index') }}/sort/created_at/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Created
                        <a href="{{ route('admin.tickets.index') }}/sort/created_at/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>
                        <a href="{{ route('admin.tickets.index') }}/sort/updated_at/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Updated
                        <a href="{{ route('admin.tickets.index') }}/sort/updated_at/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                </tr>
                <? foreach($tickets as $k => $v): ?>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td valign="top" width="1">
                            #{{ $v->id }}
                        </td>
                        <td valign="top">
                            {{ trim(substr($v['title'],0,40))}}<? if(strlen($v['title'])>40): ?>...<? endif; ?>
                        </td>
                        <td>
                            {{ $v->fname }} {{ $v->lname }}
                        </td>
                        <td>
                            {{ $v->assigneefname }} {{ $v->assigneelname }}
                        </td>
                        <td>
                            {{ $v->percentComplete }}%
                        </td>
                        <td valign="top">
                            {{ ucwords(strtolower($v->priority)) }}
                        </td>
                        <td valign="top">
                            {{ ucwords(strtolower($v->status)) }}
                        </td>
                        <td valign="top">
                            {{ displayFromStringDateTime($v->created_at) }}
                        </td>
                        <td valign="top">
                            {{ displayFromStringDateTime($v->updated_at) }}
                        </td>
                    </tr>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td></td>
                        <td colspan="8" valign="top">
                            {{ trim(substr($v->description,0,100)) }}<? if(strlen($v['description'])>100): ?>...<? endif; ?>
                        </td>
                    </tr>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td></td>
                        <td colspan="8" valign="top">
                            <a href="{{ route('admin.tickets.edit',$v['id']) }}" class="btn btn-default">Details</a>
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>
        </div>
    <? else: ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                There are no tickets to display.
            </div>
        </div>
    <? endif; ?>
    <? if(method_exists($tickets,'links')): ?>
        {{ $tickets->links() }}
    <? endif; ?>
@endsection
