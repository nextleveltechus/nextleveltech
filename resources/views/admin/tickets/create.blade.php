@extends('common/layout')

@section('content')
    {{ Form::open(['url' => route('admin.tickets.store'), 'method' => 'post', 'files' => TRUE]) }}
        @csrf
        <div class="row">
    		<div class="col-xs-12">
        		<div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', old('title')??@$ticket['title'], ['class' => 'form-control', 'id' => 'title']) }}
                </div>
            </div>
        </div>
        <div class="row">
    		<div class="col-xs-12">
        		<div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', old('description')??@$ticket->description, ['class' => 'form-control', 'id' => 'desription']) }}
        		</div>
            </div>
        </div>
        <div class="row">
    		<div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('type', 'Type') }}
                    {{ Form::select('type', \App\Ticket::TYPE, old('type')??@$ticket->type, ['class' => 'form-control', 'id' => 'type']) }}
        		</div>
            </div>
    		<div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('status', 'Status') }}
                    {{ Form::select('status', \App\Ticket::STATUS, old('status')??@$ticket->status, ['class' => 'form-control', 'id' => 'status']) }}
        		</div>
            </div>
            <div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('priority', 'Priority') }}
                    {{ Form::select('priority', \App\Ticket::PRIORITY, old('priority')??@$ticket->priority, ['class' => 'form-control', 'id' => 'priority']) }}
        		</div>
            </div>
            <div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('assigneeId', 'Assignee') }}
                    {{ Form::select('assigneeId', \App\User::system_admins(), old('assigneeId')??1, ['class' => 'form-control', 'id' => 'assigneeId']) }}
        		</div>
            </div>
            <div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('startDate', 'Start Date') }}
                    {{ Form::text('startDate', old('startDate')??@$ticket->startDate, ['class' => 'form-control datepicker', 'id' => 'startDate']) }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('endDate', 'End Date') }}
                    {{ Form::text('endDate', old('endDate')??@$ticket->endDate, ['class' => 'form-control datepicker', 'id' => 'endDate']) }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
        		<div class="form-group">
                    {{ Form::label('estimatedTime', 'Estimated Time') }}
                    <div class="input-group">
                        {{ Form::text('estimatedTime', old('estimatedTime')??@$ticket->estimatedTime, ['class' => 'form-control', 'id' => 'estimatedTime']) }}
                        <div class="input-group-addon">Hours</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('percentComplete', 'Percent Complete') }}
                    {{ Form::select('percentComplete', \App\Common::PERCENTS, old('percentComplete')??@$ticket->percentComplete, ['class' => 'form-control', 'id' => 'percentComplete']) }}
        		</div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('fileTitle', 'File Title') }}
                    {{ Form::text('fileTitle', old('fileTitle')??@$ticket->fileTitle, ['class' => 'form-control', 'id' => 'fileTitle']) }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('userfile', 'File Upload') }}
                    {{ Form::file('userfile', ['class' => 'form-control', 'id' => 'userfile']) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                    <a href="{{ route('admin.tickets.index') }}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection
