@extends('common/layout')

@section('content')
    <h3 class="text-center">Details for Ticket #{{ $ticket->id }}</h3>
    <div class="row">
    	<div class="col-xs-12">
    		<a href="" class="btn btn-default" id="updateFormTrigger">Toggle Update</a>
    	</div>
    </div>
    <br />
    <div class="well">
    	<div class="row">
    		<div class="col-xs-12">
    			<div><strong>Title</strong></div>
    			<div>{{ $ticket['title'] }}</div>
    		</div>
    	</div>
    	<br />
    	<div class="row">
    		<div class="col-xs-12">
    			<div><strong>Description</strong></div>
    			<div><pre>{{ $ticket->description }}</pre></div>
    		</div>
    	</div>
    	<br />
    	<div class="row">
            <? if($ticket->assigneefname || $ticket->assigneelname): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Assignee</strong>
    			{{ $ticket->assigneefname }} {{ $ticket->assigneelname }}
    		</div>
            <? endif; ?>
            <? if($ticket->fname || $ticket->lname): ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Created By</strong>
    			{{ $ticket->fname }} {{ $ticket->lname }}
    		</div>
            <? endif; ?>
            <? if($ticket->priority): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Priority</strong>
    			{{ ucwords(strtolower($ticket->priority)) }}
    		</div>
            <? endif; ?>
            <? if($ticket->created_at): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Created</strong>
    			{{ displayFromStringDateTime($ticket->created_at) }}
    		</div>
            <? endif; ?>
            <? if($ticket->updated_at): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Updated</strong>
    			{{ displayFromStringDateTime($ticket->updated_at) }}
    		</div>
            <? endif; ?>
            <? if($ticket->type): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Type</strong>
    			{{ ucwords(strtolower($ticket->type)) }}
    		</div>
            <? endif; ?>
            <? if($ticket->status): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Status</strong>
    			{{ ucwords(strtolower($ticket->status)) }}
    		</div>
            <? endif; ?>
            <? if($ticket->startDate): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Start Date</strong>
    			{{ displayFormattedDate($ticket->startDate) }}
    		</div>
            <? endif; ?>
            <? if($ticket->endDate): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>End Date</strong>
    			{{ displayFormattedDate($ticket->endDate) }}
    		</div>
            <? endif; ?>
            <? if($ticket->estimatedTime): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Estimated Time</strong>
    			{{ $ticket->estimatedTime }} Hours
    		</div>
            <? endif; ?>
            <? if($ticket->percentComplete): ?>
    		<div class="col-xs-12 col-sm-6 col-md-4">
    			<strong>Percent Complete</strong>
    			{{ $ticket->percentComplete }}%
    		</div>
            <? endif; ?>
    	</div>
        <? if(!$files->isEmpty()): ?>
        	<div class="row">
        		<div class="col-xs-12">
        			<br />
        			<div><strong>Files</strong></div>
        			<? foreach($files as $k => $v): ?>
                        <div>
                            <? if(in_array($v->file_ext,['png','jpg','gif','jpeg'])): ?>
                                <a href="" data-toggle="modal" data-target="#img{{ $v->id }}">{{ $v['title'] }}</a>
                            <? else: ?>
                                {{ $v['title'] }}
                            <? endif; ?>
                            - <a href="{{ route('api.files.download',$v->id) }}">{{ $v->raw_name }}.{{ $v->file_ext }}</a>
            				<div class="modal fade text-center" tabindex="-1" role="dialog" id="img{{ $v->id }}">
            					<div class="modal-dialog" role="document">
            						<div class="modal-content">
            							<div class="modal-header">
            								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            							</div>
            							<div class="modal-body">
            								<img src="{{ route('api.files.show',$v->id) }}" />
            							</div>
            						</div>
            					</div>
            				</div>
                        </div>
        			<? endforeach; ?>
        		</div>
        	</div>
    	<? endif; ?>
    </div>

    {{ Form::open(['url' => route('admin.tickets.update',$ticket->id), 'method' => 'put', 'files' => TRUE]) }}
        <div id="updateForm">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        {{ Form::label('response', 'New Response') }}&nbsp;&nbsp;<a href="" id="moreTrigger">More</a>
                        {{ Form::textarea('response', old('response'), ['class' => 'form-control', 'id' => 'response']) }}
                    </div>
                </div>
            </div>
            @csrf
            <div id="more">
                <div class="row">
            		<div class="col-xs-12">
                		<div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', old('title')??@$ticket['title'], ['class' => 'form-control', 'id' => 'title']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
            		<div class="col-xs-12">
                		<div class="form-group">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::textarea('description', old('description')??@$ticket->description, ['class' => 'form-control', 'id' => 'desription']) }}
                		</div>
                    </div>
                </div>
            </div>
            <div class="row">
        		<div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('type', 'Type') }}
                        {{ Form::select('type', \App\Ticket::TYPE, old('type')??@$ticket->type, ['class' => 'form-control', 'id' => 'type']) }}
            		</div>
                </div>
        		<div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('status', 'Status') }}
                        {{ Form::select('status', \App\Ticket::STATUS, old('status')??@$ticket->status, ['class' => 'form-control', 'id' => 'status']) }}
            		</div>
                </div>
                <div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('priority', 'Priority') }}
                        {{ Form::select('priority', \App\Ticket::PRIORITY, old('priority')??@$ticket->priority, ['class' => 'form-control', 'id' => 'priority']) }}
            		</div>
                </div>
                <div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('assigneeId', 'Assignee') }}
                        {{ Form::select('assigneeId', \App\User::system_admins(), old('assigneeId')??@$ticket->assigneeId, ['class' => 'form-control', 'id' => 'assigneeId']) }}
            		</div>
                </div>
                <div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('startDate', 'Start Date') }}
                        {{ Form::text('startDate', old('startDate')??@displayFormattedDate($ticket->startDate), ['class' => 'form-control datepicker', 'id' => 'startDate']) }}
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('endDate', 'End Date') }}
                        {{ Form::text('endDate', old('endDate')??@displayFormattedDate($ticket->endDate), ['class' => 'form-control datepicker', 'id' => 'endDate']) }}
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
            		<div class="form-group">
                        {{ Form::label('estimatedTime', 'Estimated Time') }}
                        <div class="input-group">
                            {{ Form::text('estimatedTime', old('estimatedTime')??@$ticket->estimatedTime, ['class' => 'form-control', 'id' => 'estimatedTime']) }}
                            <div class="input-group-addon">Hours</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('percentComplete', 'Percent Complete') }}
                        {{ Form::select('percentComplete', \App\Common::PERCENTS, old('percentComplete')??@$ticket->percentComplete,['class' => 'form-control', 'id' => 'percentComplete']) }}
            		</div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <hr />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('fileTitle', 'File Title') }}
                        {{ Form::text('fileTitle', old('fileTitle')??@$ticket->fileTitle, ['class' => 'form-control', 'id' => 'fileTitle']) }}
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('userfile', 'File Upload') }}
                        {{ Form::file('userfile', ['class' => 'form-control', 'id' => 'userfile']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                        <a href="{{ route('admin.tickets.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

    <? if(!empty($ticketResponses)): ?>
        <? foreach($ticketResponses as $k => $v): ?>
            <div class="well">
            	<div class="row">
            		<div class="col-xs-6">
            			<h4>{{ displayFromStringDateTime($v->created_at) }}</h4>
            		</div>
            		{{ controlLinks($v) }}
            	</div>

            	{{ listArray($v['updates']) }}

            	<div class="row">
                    <div class="col-xs-12">
                        <pre>{{ $v->response }}</pre>
                    </div>
                </div>
            	<div class="row responseDescription" id="responseDescription_{{ $v->id }}"><div class="col-xs-12">
                    {{ Form::open(['url' => route('admin.ticketresponses.update',$v->id), 'method' => 'put']) }}
                        @csrf
            			<br />
                        {{ Form::label('editDescription', 'Edit Response') }}
                        {{ Form::textarea('response', old('response')??@$v['response'], ['class' => 'form-control', 'id' => 'response']) }}
            			<br />
                        {{ Form::label('isVisible', 'Visibility') }}
                        {{ Form::select('isVisible', \App\TicketResponse::ISVISIBLE, old('isVisible')??@$v->isVisible, ['class' => 'form-control', 'id' => 'isVisible']) }}
            			<br />
            			<div class="form-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
            				<a href="" id="editResponseCancel_{{ $v->id }}" role="button" class="btn btn-default editResponseCancel">Cancel</a>
            			</div>
            		{{ Form::close() }}
            	</div></div>
            	<div class="row"><div class="col-xs-12 text-right">
            		<div><strong>{{ isVisibleText($v->isVisible) }}</strong></div>
            		<em>{{ $v->fname }} {{ $v->lname }}</em>
            	</div></div>
            </div>
        <? endforeach; ?>
    <? endif; ?>

    <script language="javascript">
    	$(document).ready(function() {
    		$("#updateFormTrigger").click(function(e){
    			e.preventDefault();
    			$("#updateForm").animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$("#moreTrigger").click(function(e){
    			e.preventDefault();
    			$("#more").animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$(".editResponse").click(function(e){
    			e.preventDefault();
    			id = $(this).attr('id');
    			$("#responseDescription_"+id).animate({ opacity: 'toggle', height: 'toggle' });
    		});

    		$(".editResponseCancel").click(function(e){
    			e.preventDefault();
                id = $(this).attr('id').split('_');
    			$("#responseDescription_"+id[1]).animate({ opacity: 'toggle', height: 'toggle' });
    		});
    	});
    </script>
@endsection
