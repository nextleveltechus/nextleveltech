@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            {{ Form::open(['url' => $url, 'method' => @$method]) }}
                @csrf
                <div class="form-group">
    				{{ Form::label('fname', 'First Name') }}
    				{{ Form::text('fname', old('fname')??@$contact->fname, ['class' => 'form-control', 'id' => 'fname']) }}
    				<p class="help-block">
    					Your first name.<br />
    					(Limit 100 characters)
    				</p>
    			</div>
    			<div class="form-group">
    				{{ Form::label('lname', 'Last Name') }}
    				{{ Form::text('lname', old('lname')??@$contact->lname, ['class' => 'form-control', 'id' => 'lname']) }}
    				<p class="help-block">
    					Your last name.<br />
    					(Limit 100 characters)
    				</p>
    			</div>

                <div class="form-group">
    				{{ Form::label('company', 'Company') }}
    				{{ Form::text('company', old('company')??@$contact->company, ['class' => 'form-control', 'id' => 'company']) }}
    				<p class="help-block">
    					Your company name.<br />
    					(Limit 100 characters)
    				</p>
    			</div>

                <div class="form-group">
    				{{ Form::label('email', 'Email') }}
    				{{ Form::text('email', old('email')??@$contact->email, ['class' => 'form-control', 'id' => 'email']) }}
    				<p class="help-block">
    					Your email address.<br />
    					(Limit 100 characters)
    				</p>
    			</div>
                <div class="form-group">
    				{{ Form::label('phone', 'Phone') }}
    				{{ Form::text('phone', old('phone')??@$contact->phone, ['class' => 'form-control', 'id' => 'phone']) }}
    				<p class="help-block">
    					Your phone number.<br />
    					(Limit 100 characters)
    				</p>
    			</div>

                <div class="form-group">
    				{{ Form::label('street1', 'Street 1') }}
    				{{ Form::text('street1', old('street1')??@$contact->street1, ['class' => 'form-control', 'id' => 'street1']) }}
    				<p class="help-block">
    					Your address.<br />
    					(Limit 100 characters)
    				</p>
    			</div>
                <div class="form-group">
    				{{ Form::label('street2', 'Street 2') }}
    				{{ Form::text('street2', old('street2')??@$contact->street2, ['class' => 'form-control', 'id' => 'street2']) }}
                    <p class="help-block">
    					(Limit 100 characters)
    				</p>
    			</div>
                <div class="form-group">
    				{{ Form::label('zipcode', 'Zip Code') }}
    				{{ Form::text('zipCode', old('zipCode')??@$contact->zipCode, ['class' => 'form-control', 'id' => 'zipcode']) }}
                    <p class="help-block">
    					(Limit 100 characters)
    				</p>
    			</div>
                <div class="form-group">
    				{{ Form::label('city', 'City') }}
    				{{ Form::text('city', old('city')??@$contact->city, ['class' => 'form-control', 'id' => 'city']) }}
                    <p class="help-block">
    					(Limit 100 characters)
    				</p>
    			</div>
                <div class="form-group">
    				{{ Form::label('state', 'State') }}
    				{{ Form::select('state', $states, old('state')??@$contact->state, ['class' => 'form-control', 'id' => 'state']) }}
    			</div>
                <div class="form-group">
                    {{ Form::label('notes', 'Notes') }}
                    {{ Form::textarea('notes', old('notes')??@$contact->notes, ['class' => 'form-control', 'id' => 'notes', 'rows' => '10']) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                    <a href="{{ route('admin.contacts.index') }}" class="btn btn-default">Cancel</a>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
