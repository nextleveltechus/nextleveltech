@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8 search-col">
            <form action="{{ route('admin.contacts.searchQuick') }}" method="post" accept-charset="utf-8">
                <div class="input-group">
                    <input type="text" name="search" value="" class="form-control" id="search" placeholder="Search">
                    <div class="input-group-btn">
                        @csrf
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <? if(method_exists($contacts,'links')): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action="{{ route('admin.contacts.saveNumPerPage') }}" method="post">
                    <div class="input-group">
                        <span class="input-group-addon">Show</span>
                        @csrf
                        {{ Form::select('numPerPage'
                        ,\App\Common::NUMPERPAGE
                        ,$numPerPage
                        ,["class" => 'numPerPage form-control']) }}
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-sm-6 search-col pull-right right">
                <span style="float:right;">{{ $contacts->links() }}</span>
            </div>
        </div>
    <? endif; ?>

    <? if(!empty($search)): ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                Searching for <i><?=$search?></i>
            </div>
        </div>
        <br />
    <? endif; ?>

    <div class="row">
        <div class="col-xs-12 text-center">
            <a href="{{ route('admin.contacts.create') }}" class="btn btn-default">Add New Contact</a>
        </div>
    </div>

    <br />

    <? if(!$contacts->isEmpty()): ?>
        <div class="table-responsive no-border">
            <table class="table">
                <tr class="tableStripe">
                    <th></th>
                    <th>
                        <a href="{{ route('admin.contacts.index') }}/sort/fname/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Name
                        <a href="{{ route('admin.contacts.index') }}/sort/fname/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>
                        <a href="{{ route('admin.contacts.index') }}/sort/company/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Company
                        <a href="{{ route('admin.contacts.index') }}/sort/company/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>
                        <a href="{{ route('admin.contacts.index') }}/sort/email/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                        Email
                        <a href="{{ route('admin.contacts.index') }}/sort/email/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                    </th>
                    <th>
                        Phone
                    </th>
                    <th>
                        Location
                    </th>
                </tr>
                <? foreach($contacts as $k => $v): ?>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td valign="top" width="1">
                            <?=@$start+$k+1?>.
                        </td>
                        <td valign="top">
                            {{ $v->fname }} {{ $v->lname }}
                        </td>
                        <td valign="top">
                            {{ $v->company }}
                        </td>
                        <td valign="top">
                            {{ $v->email }}
                        </td>
                        <td>
                            {{ $v->phone }}
                        </td>
                        <td>
                            {{ display_location($v) }}
                        </td>
                    </tr>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td></td>
                        <td colspan="5">
                            {{ $v->notes }}
                        </td>
                    </tr>
                    <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                        <td colspan="9" valign="top">
                            <a href="{{ route('admin.contacts.edit', $v->id) }}" class="btn btn-default">Edit</a>

                            <a class="btn btn-default" data-toggle="modal" data-target="#delete{{ $v->id }}">Delete</a>

                  			<div class="modal fade text-center" tabindex="-1" role="dialog" id="delete{{ $v->id }}">
                  				<div class="modal-dialog" role="document">
                  					<div class="modal-content">
                  						<div class="modal-header">
                                            {{ Form::button('x',['class' => 'close'
                                                ,'data-dismiss' => 'modal'
                                                ,'aria-hidden' => 'true']) }}

                  							<h4 class="modal-title"><span class="glyphicon glyphicon-warning-sign"></span> Warning</h4>
                  						</div>
                  						<div class="modal-body text-center">
                  							<h4>Are you sure you want to delete this contact?</h4>
                  							<h4>This action cannot be undone.</h4>
                  						</div>
                  						<div class="modal-footer">
                                            {{ Form::open(['url' => route('admin.contacts.destroy',$v->id), 'style' => 'display:inline;', 'method' => 'post']) }}
                                                @method('DELETE')
                                                {{ Form::submit('Delete', ['class' => 'btn btn-default']) }}
                                            {{ Form::close() }}
                                            <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                  						</div>
                  					</div>
                  				</div>
                  			</div>
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>
        </div>
    <? else: ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                There are no contacts to display.
            </div>
        </div>
    <? endif; ?>
    <? if(method_exists($contacts,'links')): ?>
        {{ $contacts->links() }}
    <? endif; ?>
@endsection
