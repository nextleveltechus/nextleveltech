@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8 search-col">
            <form action="{{ route('admin.users.searchQuick') }}" method="post" accept-charset="utf-8">
                <div class="input-group">
                    <input type="text" name="keyword" value="" class="form-control" id="keyword" placeholder="Search">
                    <div class="input-group-btn">
                        @csrf
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <? if(method_exists($users,'links')): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action='{{ route('admin.users.showOnly') }}' method="post">
                    <div class="input-group">
                        <span class="input-group-addon">Show Only</span>
                        @csrf
                        <select name="adminUsersShowOnly" class="form-control showOnly">
                            <option value="">All</option>
                            <? foreach(\App\User::TYPE as $k => $v): ?>
                                <option value="<?=$v?>"<? if($v == $adminUsersShowOnly): ?> SELECTED<? endif; ?>><?=ucwords(strtolower($v))?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
      		<div class="col-xs-12 col-sm-4 search-col">
      			<form action='{{ route('admin.users.saveNumPerPage') }}' method="post">
      				<div class="input-group">
      					<span class="input-group-addon">Show</span>
                        @csrf
      					{{ Form::select('numPerPage'
                            ,\App\Common::NUMPERPAGE
                            ,$numPerPage
                            ,["class" => 'numPerPage form-control']) }}
      				</div>
      			</form>
      		</div>

      		<div class="col-xs-12 col-sm-6 search-col pull-right right">
                <span style="float:right;">{{ $users->links() }}</span>
            </div>
        </div>
    <? endif; ?>

    <? if(!empty($keyword)): ?>
      <div class="row">
    		<div class="col-xs-12 text-center">
    			Searching for <i><?=$keyword?></i>
    		</div>
      </div>
      <br />
    <? endif; ?>

    <div class="row">
        <div class="col-xs-12 text-center search-col">
            <a href="{{ route('admin.users.add') }}" class="btn btn-default">Add New User</a>
        </div>
    </div>

    <div class="table-responsive no-border">
    <table class="table">
        <tr class="tableStripe">
            <th></th>
            <th>
                <a href="{{ route('admin.users') }}/sort/username/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                Username
                <a href="{{ route('admin.users') }}/sort/username/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
            </th>
            <th>
                <a href="{{ route('admin.users') }}/sort/email/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                Email
                <a href="{{ route('admin.users') }}/sort/email/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
            </th>
            <th>
                <a href="{{ route('admin.users') }}/sort/fname/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                First Name
                <a href="{{ route('admin.users') }}/sort/fname/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
            </th>
            <th>
                <a href="{{ route('admin.users') }}/sort/lname/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                Last Name
                <a href="{{ route('admin.users') }}/sort/lname/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
            </th>
            <th>Type</th>
            <th>Status</th>
            <th>
                <a href="{{ route('admin.users') }}/sort/created_at/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                Created
                <a href="{{ route('admin.users') }}/sort/created_at/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
            </th>
        </tr>
    <? foreach($users as $k => $v): ?>
        <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
            <td width="1"><?= $k + 1 + $start ?></td>
            <td>{{ $v->username }}</td>
            <td>{{ $v->email }}</td>
            <td>{{ $v->fname }}</td>
            <td>{{ $v->lname }}</td>
            <td>{{ ucwords(strtolower($v->type)) }}</td>
            <td>{{ ucwords(strtolower($v->status)) }}</td>
            <td>{{ displayFromStringDateTime($v->created_at) }}</td>
        </tr>
        <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
            <td></td>
            <td colspan="8">
                <? if($v['id'] != Auth::user()->id): ?>
                    <a href="{{ route('admin.users.edit') }}/id/{{ $v->id }}" class="btn btn-default">Edit</a>
                <? endif; ?>
                <? if($v->status == ucwords(strtoupper(\App\User::STATUS['ACTIVE'])) && $v->id != Auth::user()->id): ?>
                    <form action="{{ route('auth.login.auto') }}" method="post" style="display:inline;">
                        @csrf
                        {{ Form::hidden('user',serialize($v->toArray())) }}
                        {{ Form::submit('Login',["class" => "btn btn-default"]) }}
                    {{ Form::close() }}
                <? endif; ?>
            </td>
        </tr>
    <? endforeach; ?>
    </table>
    </div>
    <? if(method_exists($users,'links')): ?>
        {{ $users->links() }}
    <? endif; ?>
@endsection
