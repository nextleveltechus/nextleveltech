Hello! Welcome to {{ config('app.name') }}
<br /><br />
Please log into {{ config('app.name') }} with the following credentials:
<br /><br />
USERNAME: {{ $username }}<br />
PASSWORD: {{ $password }}
<br /><br />
<a href="{{ displayProtocol() }}{{ $_SERVER['HTTP_HOST'] }}/login">CLICK HERE</a> to access {{ config('app.name') }}.</a>
<br />
{{ displayProtocol() }}{{ $_SERVER['HTTP_HOST'] }}/login
<br /><br />
Thank you!
<br /><br />
{{ config('app.name') }} Team
