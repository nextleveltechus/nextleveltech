@extends('common/layout')

@section('content')
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.users') }}" class="btn btn-default btn-block">Manage Users</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.tickets.index') }}" class="btn btn-default btn-block">Manage Tickets</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.contacts.index') }}" class="btn btn-default btn-block">Manage Contacts</a>
        </div>
    </div>
    <?/*
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.job.index') }}" class="btn btn-default btn-block">View Jobs</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.billing.index') }}" class="btn btn-default btn-block">View Billing</a>
        </div>
    </div>
    */?>
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('admin.emails') }}" class="btn btn-default btn-block">Support Emails</a>
        </div>
    </div>

    <br /><br /><br />
@endsection
