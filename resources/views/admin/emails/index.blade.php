@extends('common/layout')

@section('content')
    <? if($emails->isEmpty()): ?>
        <br /><br />
        <div class="row">
            <div class="col-xs-12 text-center">
                No emails have been sent to support.
            </div>
        </div>
        <br /><br /><br /><br />
    <? else: ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-8 search-col">
                <form action="{{ route('admin.emails.searchQuick') }}" method="post" accept-charset="utf-8">
                    <div class="input-group">
                        <input type="text" name="keyword" value="" class="form-control" id="keyword" placeholder="Search">
                        <div class="input-group-btn">
                            @csrf
                            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <? if(method_exists($emails,'links')): ?>
            <div class="row">
          		<div class="col-xs-12 col-sm-4 search-col">
          			<form action='{{ route('admin.emails.saveNumPerPage') }}' method="post">
          				<div class="input-group">
          					<span class="input-group-addon">Show</span>
                            @csrf
          					{{ Form::select('numPerPage'
                                ,\App\Common::NUMPERPAGE
                                ,$numPerPage
                                ,["class" => 'numPerPage form-control']) }}
          				</div>
          			</form>
          		</div>

          		<div class="col-xs-12 col-sm-6 search-col pull-right right">
                    <span style="float:right;">{{ $emails->links() }}</span>
                </div>
            </div>
        <? endif; ?>

        <? if(!empty($keyword)): ?>
          <div class="row">
        		<div class="col-xs-12 text-center">
        			Searching for <i><?=$keyword?></i>
        		</div>
          </div>
          <br />
        <? endif; ?>

        <div class="table-responsive no-border">
        <table class="table">
            <tr class="tableStripe">
                <th></th>
                <th>
                    <a href="{{ route('admin.emails') }}/sort/email/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                    Sent From
                    <a href="{{ route('admin.emails') }}/sort/email/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                </th>
                <th>
                    <a href="{{ route('admin.emails') }}/sort/name/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                    Name
                    <a href="{{ route('admin.emails') }}/sort/name/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                </th>
                <th>
                    Phone
                </th>
                <th>
                    Message
                </th>
                <th>
                    <a href="{{ route('admin.emails') }}/sort/created_at/order/asc" class="glyphicon glyphicon-sort-by-alphabet"></a>
                    Sent At
                    <a href="{{ route('admin.emails') }}/sort/created_at/order/desc" class="glyphicon glyphicon-sort-by-alphabet-alt"></a>
                </th>
            </tr>
        <? foreach($emails as $k => $v): ?>
            <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                <td width="1"><?=$start+$k+1?>.</td>
                <td>{{ $v->from }}</td>
                <td>{{ $v->name }}</td>
                <td>{{ $v->phone }}</td>
                <td>{{ substr($v->message,0,100) }}</td>
                <td>{{ displayFromStringDateTime($v->created_at) }}</td>
            </tr>
            <tr<? if($k%2!=0): ?> class="tableStripe"<? endif; ?>>
                <td></td>
                <td colspan="6">
                    <a href="{{ route('admin.emails.show',$v->id) }}" class="btn btn-default">More Info</a>
                </td>
            </tr>
        <? endforeach; ?>
        </table>
        </div>
        <? if(method_exists($emails,'links')): ?>
            {{ $emails->links() }}
        <? endif; ?>
    <? endif; ?>
@endsection
