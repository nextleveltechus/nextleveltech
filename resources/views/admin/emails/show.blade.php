@extends('common/layout')

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        {{ Form::open(['url' => route('admin.emails.destroy')]) }}
            @csrf
            {{ Form::hidden('id',$email['id']) }}
            <div class="form-group">
                <label for="created_at">Sent at</label> {{ displayFromStringDateTime($email->created_at) }}
            </div>
            <div class="form-group">
                <label for="to">Sent to</label>
                <div id="to">{{ str_replace(',',', ',$email->to) }}</div>
            </div>
            <div class="form-group">
                <label for="from">From</label> {{ $email->name }} ({{ $email->from }})
            </div>
            <div class="form-group">
                <label for="from">Phone</label> {{ $email->phone }}
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <div id="message">{{ $email->message }}</div>
            </div>

            <div class="form-group text-right">
                <a href="" id="trigger-auth" class="btn btn-default">More</a>
                {{ Form::submit('Delete',["class" => "btn btn-default"]) }}
                <a href="{{ route('admin.emails') }}" class="btn btn-default">Cancel</a>
            </div>

            <div id="container-auth" class="more">
                <? if(!empty($email['auth'])): ?>
                    <div class="form-group">
                        <label for="auth">Auth</label>
                        <div id="auth"><pre><? print_r(@unserialize($email->auth)) ?></pre></div>
                    </div>
                <? endif; ?>
                <? if(!empty($email['session'])): ?>
                    <div class="form-group">
                        <label for="session">Session</label>
                        <div id="session"><pre><? print_r(@unserialize($email->session)) ?></pre></div>
                    </div>
                <? endif; ?>
                <? if(!empty($email['request'])): ?>
                    <div class="form-group">
                        <label for="request">Request</label>
                        <div id="request"><pre><? print_r(@unserialize($email->request)) ?></pre></div>
                    </div>
                <? endif; ?>
                <? if(!empty($email['cookie'])): ?>
                    <div class="form-group">
                        <label for="cookie">Cookie</label>
                        <div id="cookie"><pre><? print_r(@unserialize($email->cookie)) ?></pre></div>
                    </div>
                <? endif; ?>
                <? if(!empty($email['server'])): ?>
                    <div class="form-group">
                        <label for="server">Server</label>
                        <div id="server"><pre><? print_r(@unserialize($email->server)) ?></pre></div>
                    </div>
                <? endif; ?>
            </div>
        {{ Form::close() }}
    </div>
</div>

<script language="javascript">
    $(document).ready(function() {
        $("#trigger-auth").click(function(e){
            e.preventDefault();
            $("#container-auth").animate({ opacity: 'toggle', height: 'toggle' });
        });
    });
</script>
@endsection
