@extends('common/layout')

@section('content')
    <div class="help-block col-sm-10 col-sm-offset-1">
        <h4 class="page-title">Account</h4>
        <div>This is your account information.  You can change the information as needed.</div>
        <br /><br /><br />
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            {{ Form::open() }}
                @csrf
                <div class="form-group">
                    {{ Form::label('username', 'Username') }}
                    {{ Form::text('username', old('username')??@$user->username, ['class' => 'form-control']) }}
                    <p class="help-block">
                        The username you will use to login.<br />
                        (Limit 6-25 alphanumeric characters)
                    </p>
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::text('password', old('password'), ['class' => 'form-control']) }}
                    <p class="help-block">
                        The password you will use to login.<br />
                        (Limit 6-25 characters, Must contain a capital letter, a lowercase letter, a number, and a special character)
                    </p>
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', old('email')??@$user->email, ['class' => 'form-control']) }}
                    <p class="help-block">
                        Your email address. This will be used for password recovery.<br />
                        (Valid email address, Limit 100 characters)
                    </p>
                </div>
                <div class="form-group">
                    {{ Form::label('timezone', 'Time Zone') }}
                    {{ Form::select('timezone', array_flip(\App\User::TIMEZONE), old('timezone')??@$user->timezone,
                    ['class' => 'form-control']) }}
                    <p class="help-block">
                        The time zone that you operate in.  The times on this website
                        will change depending on your time zone.
                    </p>
                </div>
                <div class="form-group">
    				{{ Form::label('fname', 'First Name') }}
    				{{ Form::text('fname', old('fname')??@$user->fname, ['class' => 'form-control']) }}
    				<p class="help-block">
    					Your first name.<br />
    					(Limit 100 characters)
    				</p>
    			</div>
    			<div class="form-group">
    				{{ Form::label('lname', 'Last Name') }}
    				{{ Form::text('lname', old('lname')??@$user->lname, ['class' => 'form-control']) }}
    				<p class="help-block">
    					Your last name.<br />
    					(Limit 100 characters)
    				</p>
    			</div>

                {{ Form::submit('Submit',["class" => "btn btn-default"]) }}
                <a href="{{ route('index') }}" class="btn btn-default">Cancel</a>
            {{ Form::close() }}
        </div>
    </div>
@endsection
