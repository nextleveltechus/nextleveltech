@extends('common/layout')

@section('content')
    <div class="help-block col-sm-10 col-sm-offset-1">
        <h4 class="page-title">Uploads</h4>
        <div>This is your personal storage space.  You can upload files as you please.</div>
        <br /><br /><br />
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8 search-col">
            <form action="{{ route('account.uploads.searchQuick') }}" method="post" accept-charset="utf-8">
                <div class="input-group">
                    <input type="text" name="search" value="" class="form-control" id="search" placeholder="Search">
                    <div class="input-group-btn">
                        @csrf
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <? if(method_exists($files,'links')): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action="{{ route('account.uploads.saveShowOnly') }}" method="post" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-addon">Show Only</span>
                        @csrf
                        <select name="type" class="form-control showOnly">
                            <option value="">All</option>
                            <? foreach(\App\File::return_type_array() as $k => $v): ?>
                                <option value="<?=$v?>"<? if($v == $showOnly): ?> SELECTED<? endif; ?>>{{ $v }}</option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-4 search-col">
                <form action="{{ route('account.uploads.saveNumPerPage') }}" method="post">
                    <div class="input-group">
                        <span class="input-group-addon">Show</span>
                        @csrf
                        {{ Form::select('numPerPage'
                        ,\App\Common::NUMPERPAGE
                        ,$numPerPage
                        ,["class" => 'numPerPage form-control']) }}
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-sm-6 search-col pull-right right">
                <span style="float:right;">{{ $files->links() }}</span>
            </div>
        </div>
    <? endif; ?>

    <? if(!empty($search)): ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                Searching for <i><?=$search?></i>
            </div>
        </div>
        <br />
    <? endif; ?>

    <div class="row text-center">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <span class="btn btn-default" id="btn_new_upload">New Upload</span>
        </div>
    </div>
    <div class="row" id="form_upload">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            {{ Form::open(['url' => '/account/uploads/store/', 'method' => 'post', 'files' => TRUE]) }}
                <div class="form-group">
                    {{ Form::label('fileTitle', 'File Title') }}
                    {{ Form::text('fileTitle', old('fileTitle'), ['class' => 'form-control', 'id' => 'fileTitle']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('userfile', 'File Upload') }}
                    {{ Form::file('userfile', ['class' => 'form-control', 'id' => 'userfile']) }}
                </div>
                <div class="form-group text-right">
                    {{ Form::submit('Submit', ['class' => 'btn btn-default']) }}
                    <span class="btn btn-default" id="btn_close">Cancel</span>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <br />

    <? if(!$files->isEmpty()): ?>
        <div class="row">
            <? foreach($files as $k => $v): ?>
                <div class="col-xs-12 col-sm-4 text-center" style="margin-top:10px;">
                    <div style="border:1px solid #CCC;overflow:hidden;padding:20px 5px 40px 5px;">
                        <div class="text-right">
                            <a class="glyphicon glyphicon-remove" data-toggle="modal" data-target="#delete{{ $v->id }}" style="margin-right:15px;color:#ef7575;">
                            </a>
                            <div class="modal fade text-center" tabindex="-1" role="dialog" id="delete{{ $v->id }}">
                  				<div class="modal-dialog" role="document">
                  					<div class="modal-content">
                  						<div class="modal-header">
                                            {{ Form::button('x',['class' => 'close'
                                                ,'data-dismiss' => 'modal'
                                                ,'aria-hidden' => 'true']) }}

                  							<h4 class="modal-title"><span class="glyphicon glyphicon-warning-sign"></span> Warning</h4>
                  						</div>
                  						<div class="modal-body text-center">
                  							<h4>Are you sure you want to delete this file?</h4>
                  						</div>
                  						<div class="modal-footer">
                                            {{ Form::open(['url' => route('account.uploads.destroy'), 'style' => 'display:inline;', 'method' => 'post']) }}
                                                {{ Form::hidden('fileId',$v->id) }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-default']) }}
                                            {{ Form::close() }}
                                            <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                  						</div>
                  					</div>
                  				</div>
                  			</div>
                        </div>
                        <div>
                            <? if(in_array($v->file_ext,['png','jpg','gif','jpeg'])): ?>
                                <a data-toggle="modal" data-target="#img{{ $v->id }}"><img src="/images/file.png" /></a>
                            <? else: ?>
                                <a href="{{ route('api.files.download',$v->id) }}"><img src="/images/file.png" /></a>
                            <? endif; ?>
                        </div>
                        <br />
                        <div>
                            <? if(in_array($v->file_ext,['png','jpg','gif','jpeg'])): ?>
                                <a data-toggle="modal" data-target="#img{{ $v->id }}">{{ substr($v['title'],0,30) }}</a>
                            <? else: ?>
                                {{ substr($v['title'],0,30) }}
                            <? endif; ?>
                        </div>
                        <div>
                            <? if(in_array($v->file_ext,['png','jpg','gif','jpeg'])): ?>
                                <a data-toggle="modal" data-target="#img{{ $v->id }}">view</a>&nbsp;&bull;
                            <? endif; ?>
                            <a href="{{ route('api.files.download',$v->id) }}">download</a>
                        </div>

        				<div class="modal fade text-center" tabindex="-1" role="dialog" id="img{{ $v->id }}">
        					<div class="modal-dialog" role="document">
        						<div class="modal-content">
        							<div class="modal-header">
        								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        							</div>
        							<div class="modal-body">
        								<img src="{{ route('api.files.show',$v->id) }}" />
        							</div>
        						</div>
        					</div>
        				</div>

                        <div>{{ displayFromStringDateTime($v->created_at) }}</div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    <? else: ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                There are no files to display.
            </div>
        </div>
    <? endif; ?>
    <? if(method_exists($files,'links')): ?>
        {{ $files->links() }}
    <? endif; ?>
@endsection
