@extends('common/layout_index')

@section('content_head')
    {!! siteErrors($errors->all()) !!}
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });

        function onSubmit(token) {
			document.getElementById("google_captcha_form").submit();
		}
    </script>

    <div class="hidden-xs">
        <div style="position:absolute;width:100%;">
            <div style="float:right;margin:10px 10px 5px 0px;">
                <i class="fas fa-mobile-alt"></i>&nbsp;&nbsp;<a href="tel:5616748000" style="color:#2A4F7B;">561.674.8000</a>
            </div>
            <div style="clear:both;">
            </div>
            <div style="float:right;margin-right:10px;">
                <i class="fas fa-paper-plane">&nbsp;</i> <a href="#contact" style="color:#2A4F7B;">{{ config('app.support_email') }}</a>
            </div>
        </div>
        <div class="container-fluid text-center">
            <object type="image/svg+xml" data="/images/logo_blue.svg" height="100">
                <img src="/images/logo_blue.png" height="100" />
            </object>
            <h3>{{ config('app.name') }}</h3>
        </div>
    </div>

    <div class="hidden-sm hidden-md hidden-lg hidden-xl">
        <div class="container-fluid text-center" style="padding-top:20px;">
            <object type="image/svg+xml" data="/images/logo_blue.svg" height="50">
                <img src="/images/logo_blue.png" height="50" />
            </object>
            <h4>{{ config('app.name') }}</h4>

            <div style="overflow:hidden;margin:0;padding:0;">
                <span style="float:left;display:inline;">
                    <i class="fas fa-mobile-alt"></i>&nbsp;&nbsp;<a href="tel:5616748000" style="color:#2A4F7B;">(561) 674-8000</a>
                </span>
                <span style="float:right;display:inline;">
                    <i class="fas fa-paper-plane">&nbsp;</i> <a href="#contact" style="color:#2A4F7B;">{{ config('app.support_email') }}</a>
                </span>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-default navbar-main" style="position:relative;top:-5px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
            data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            	<span class="sr-only">Toggle navigation</span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                {!! administration_link() !!}
                {!! services_link() !!}
                {!! support_link() !!}
                {!! logged_in_link() !!}
                {!! login_link() !!}
            </ul>
        </div>
    </nav>
@endsection

@section('content')

<?/* LARGE LAYOUT */?>
    <div class="hidden-xs">
        <br /><br />
        <div class="row text-center">
            <div class="col-xs-12">
                <h4>Where we ask the question:</h4>
                <h3 class="marker need blue">"What does your business need?"</h3>
                <h5>Whatever your business needs are, we'll quickly create a package that is right and affordable for your business.</h5>
            </div>
        </div>

        <br /><br /><br /><br /><br /><br /><br /><br />

        <div class="row text-center" id="phone-apps-top">
            <br /><br />
            <div class="col-xs-12 col-sm-6 hidden-xs index-block-recruiter">
            </div>
            <div class="col-xs-12 col-sm-6 index-block-container">
                <h3>Custom Web and Phone Apps</h3>
                <h4 style="text-align:center">
                    <br />
                    <h5 style="text-align:center;line-height:1.5;margin-right:10px;">We can facilitate special projects, custom websites, and phone apps.</h5>
                    <br />
                    <div style="line-height:1.5;">How can we help you?</div>
                    <br />
                    <a href="#phone-apps" class="btn btn-danger btn-lg"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Custom Programming</a>
                </h4>
            </div>
        </div>

        <div class="index-container-spacer"></div>
        <br /><br /><br />

        <div class="row text-center" id="server-management-top">
            <br /><br />
            <div class="col-xs-12 col-sm-6 index-block-container">
                <h3>Server Management</h3>
                <h4 style="text-align:center">
                    <div>We can handle your entire web presence.</div>
                    <br />
                    <h5 style="text-align:center;line-height:1.5;margin-right:10px;">Domain registration, DNS, SSL, website hosting, custom websites and phone apps,
                        SEO optimization, web marketing; our team can serve as your entire IT Department.</h5>
                    <br />
                    <a href="#server-management" class="btn btn-danger btn-lg"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Server Management</a>
                </h4>
            </div>
            <div class="col-xs-12 col-sm-6 index-block-job-seeker">
            </div>
        </div>

        <div class="index-container-spacer"></div>
        <br /><br /><br />

        <div class="row text-center" id="information-technology-staffing-top">
            <br /><br />
            <div class="col-xs-12 col-sm-6 hidden-xs index-block-company">
            </div>
            <div class="col-xs-12 col-sm-6 index-block-container">
                <h3>IT Staffing</h3>
                <h4 style="text-align:center">
                    <div>We have connections with the industry's top talent.</div>
                    <br />
                    <h5 style="text-align:center;line-height:1.5;margin-left:10px;">If you're looking to staff your business with the most qualified professinals, we can help.</h5>
                    <br />
                    <a href="#information-technology-staffing" class="btn btn-danger btn-lg"><i class="far fa-lightbulb"></i>&nbsp;Learn More About IT Staffing</a>
                </h4>
            </div>
        </div>

        <div class="index-container-spacer"></div>
        <br /><br /><br />

        <div class="row text-center" id="stock-supplies-top">
            <br /><br />
            <div class="col-xs-12 col-sm-6 index-block-container">
                <h3>Social Networking Presence</h3>
                <h4 style="text-align:center">
                    <br />
                    <h5 style="text-align:center;line-height:1.5;margin-right:10px;">Facebook, Twitter, Instagram, Reddit - whatever the social networking platform you desire, we can help you establish your own business pages.</h5>
                    <br />
                    <a href="#contact" class="btn btn-danger btn-lg"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Social Networking</a>
                </h4>
            </div>
            <div class="col-xs-12 col-sm-6 hidden-xs index-block-stock-supplies">
            </div>
        </div>

        <?/*<div class="index-container-spacer"></div>
        <br /><br /><br />

        <div class="row text-center" id="professional-office-cleaning-top">
            <br /><br />
            <div class="col-xs-12 col-sm-6 hidden-xs index-block-cleaner">
            </div>
            <div class="col-xs-12 col-sm-6 index-block-container">
                <h3>Professional Office Cleaning</h3>
                <h4 style="text-align:center">
                    <div>Schedule a weekly or monthly office cleaning.</div>
                    <br />
                    <h5 style="text-align:center;line-height:1.5;margin-left:10px;">
                        Our trusted cleaning staff will clean your office as you specify.  All cleaning sessions are monitored for safety, your security, and completeness.</h5>
                    <br />
                    <a href="#contact" class="btn btn-danger btn-lg"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Office Cleaning</a>
                </h4>
            </div>
        </div>*/?>
    </div>

<?/* SMALL LAYOUT */?>
    <div class="hidden-sm hidden-md hidden-lg hidden-xl">
        <br /><br />
        <div class="row text-center" style="padding:0px 10px;">
            <div class="col-xs-12">
                <h5>Where we ask the question:</h5>
                <h3 class="marker need_small blue">"What does your business need?"</h3>
                <h5>Whatever your business needs are, we'll quickly create a package that is right and affordable for your business.</h5>
            </div>
        </div>

        <div class="index-container-spacer" id="phone-apps-small"></div>

        <div class="row text-center">
            <br /><br />
            <div style="position:relative;height:300px;">
                <div class="col-xs-12 index-block-job-seeker" style="position:absolute;height:300px;">
                </div>
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;background: rgba(255, 255, 255, 0.8);">
                </div>
                <div class="col-xs-12 index-block-container" style="position:absolute;height:300px;">
                    <h3 style="background:rgba(8,70,121,.4);padding:10px 0;position:relative;top:-25px;">Custom Web and Phone Apps</h3>
                    <h4 style="text-align:left;line-height:1.5;margin-left:10px;">
                        <h5>We can facilitate special projects, custom websites, and phone apps.</h5>
                        <br />
                        <div style="text-align:center;">How can we help you?</div>
                    </h4>
                    <br />
                    <a href="#phone-apps" class="btn btn-danger btn-sm"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Custom Programming</a>
                </div>
            </div>
        </div>

        <div class="index-container-spacer" id="server-management-small"></div>

        <div class="row text-center">
            <br /><br />
            <div style="position:relative;height:300px;">
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;">
                </div>
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;background: rgba(255, 255, 255, 0.8);">
                </div>
                <div class="col-xs-12 index-block-container" style="position:absolute;height:300px;">
                    <h3 style="background:rgba(8,70,121,.4);padding:10px 0;position:relative;top:-25px;">Server Management</h3>
                    <h4 style="text-align:center;line-height:1.5;margin-right:10px;">
                        <div>We can handle your entire web presence.</div>
                        <br />
                        <h5 style="text-align:center;">We provide domain registration, DNS, SSL, website hosting, custom websites and phone apps,
                            SEO optimization, web marketing.</h5>
                        <br />
                        <a href="#server-management" class="btn btn-danger btn-sm"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Server Management</a>
                    </h4>
                </div>
            </div>
        </div>

        <div class="index-container-spacer" id="information-technology-staffing-small"></div>

        <div class="row text-center">
            <br /><br />
            <div style="position:relative;height:300px;">
                <div class="col-xs-12 index-block-recruiter" style="position:absolute;height:300px;">
                </div>
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;background: rgba(255, 255, 255, 0.8);">
                </div>
                <div class="col-xs-12 index-block-container" style="position:absolute;height:300px;">
                    <h3 style="background:rgba(8,70,121,.4);padding:10px 0;position:relative;top:-25px;">IT Staffing</h3>
                    <h4 style="text-align:left;line-height:1.5;margin-left:10px;">
                        <div style="text-align:center;">We have connections with the industries top talent.</div>
                        <br />
                        <h5>If you're looking to staff your business with the most qualified professinals, we can help.</h5>
                    </h4>
                    <br />
                    <a href="#information-technology-staffing" class="btn btn-danger btn-sm"><i class="far fa-lightbulb"></i>&nbsp;Learn More About IT Staffing</a>
                </div>
            </div>
        </div>

        <?/*<div class="index-container-spacer" id="professional-office-cleaning-small"></div>

        <div class="row text-center">
            <br /><br />
            <div style="position:relative;height:300px;">
                <div class="col-xs-12 index-block-cleaner" style="position:absolute;height:300px;">
                </div>
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;background: rgba(255, 255, 255, 0.8);">
                </div>
                <div class="col-xs-12 index-block-container" style="position:absolute;height:300px;">
                    <h3 style="background:rgba(8,70,121,.4);padding:10px 0;position:relative;top:-25px;">Professional Office Cleaning</h3>
                    <h4 style="text-align:left;line-height:1.5;margin-left:10px;">
                        <div style="text-align:center;">Schedule a weekly or monthly office cleaning.</div>
                        <br />
                        <h5>Our trusted cleaning staff will clean your office as you specify.  All cleaning sessions are monitored for safety, your security, and completeness.</h5>
                    </h4>
                    <br />
                    <a href="#contact" class="btn btn-danger btn-sm"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Professional Cleaning</a>
                </div>
            </div>
        </div>*/?>

        <div class="index-container-spacer" id="stock-supplies-small"></div>

        <div class="row text-center">
            <br /><br />
            <div style="position:relative;height:300px;">
                <div class="col-xs-12 index-block-stock-supplies" style="position:absolute;height:300px;">
                </div>
                <div class="col-xs-12 index-block-company" style="position:absolute;height:300px;background: rgba(255, 255, 255, 0.8);">
                </div>
                <div class="col-xs-12 index-block-container" style="position:absolute;height:300px;">
                    <h3 style="background:rgba(8,70,121,.4);padding:10px 0;position:relative;top:-25px;">Social Networking</h3>
                    <h4 style="text-align:left;line-height:1.5;margin-left:10px;">
                        <div style="text-align:center;">Facebook, Twitter, Instagram, Reddit</div>
                        <br />
                        <h5>Whatever the social networking platform you desire, we can help you establish your own business pages.</h5>
                    </h4>
                    <br />
                    <a href="#contact" class="btn btn-danger btn-sm"><i class="far fa-lightbulb"></i>&nbsp;Learn More About Social Networking</a>
                </div>
            </div>
        </div>
    </div>

    <div class="index-container-spacer"></div>
    <br /><br /><br />

    <div class="contaienr-fluid" style="margin:0px 40px;line-height:1.5;">
        <div class="row" id="phone-apps">
            <div class="col-xs-12">
                <h3>Custom Programming Details</h3>
                <h5 style="line-height:1.5">
                    <div>{{ config('app.name') }} is excited to hear about your new project.</div>
                    <ul>
                        <br />
                        <li>Initial Project Scope - We will schedule an hour phone or skype conversation to outline your project. This conversation is to get a very detailed plan of your ideas, aspirations and intentions to decide the best way we can help you achieve them. During this conversation we will tell you which tools we'll use to complete the job and give an estimate of how long it will take.</li>
                        <br />
                        <li>Phone Apps - Phone apps can be done in X Code (IOS), Android Studio, or a Hybrid Cordova solution. The precise method we use to create your app will be discussed and agreed upon in the initial meeting. Currently, there is an added cost of $100 for iTunes and $25 for Google Play to get these apps uploaded to the store.</li>
                        <br />
                        <li>Custom Website – Websites, whether custom or Wordpress installs, will be completed by our in house staff.</li>
                        <br />
                    </ul>
                    <div class="text-center"><a href="#contact" class="btn btn-danger">Contact us for a Free Quote!</a></div>
                </h5>
            </div>
        </div>

        <div class="index-container-spacer"></div>

        <div class="row" id="server-management">
            <div class="col-xs-12">
                <h3>Server Management Details</h3>
                <h5 style="line-height:1.5">
                    <div>{{ config('app.name') }} will manage your IT goals from start to finish.</div>
                    <ul>
                        <br />
                        <li>Domain Registration - We will handle this for you, for as many domains as you wish to own. If you favor a particular domain registrar, we will use it. You will be provided with a username and password, ensuring that you always have control of your domains.</li>
                        <br />
                        <li>DNS - Your DNS will most likely be handled by your domain registrar, but if specific DNS servers need to be used, we will use your preferred DNS servers.</li>
                        <br />
                        <li>SSL Certificates - Certificates will be purchased and installed as needed. You will most likely be provided with a username and password associated with this account beacuase they are usually purchased separate from your domain registrar.</li>
                        <br />
                        <li>Web Hosting - All webhosting is purchased and managed through the Google Cloud Platform and currently comes with a $300 free trial. We will give you your username, password and SSH access if needed.</li>
                        <br />
                        <li>SEO Optimization - Once your website is up and running, our team will optimize your SEO with best practices currently outlined by Google. Analytics will be installed, and an analytics account will be given to you. </li>
                        <br />
                        <li>Web Marketing - If you are interested in getting your brand out there, we'll suggest ways that you can get more exposure. Social media accounts may also be created.</li>
                        <br />
                        <li>24 Hour Support - If something goes wrong along the way, or you have any questions or concerns, our support team is here for you around the clock.</li>
                        <br />
                        <li>Scheduled Backups - Backups will be created every night at 2am for all databases and important server configuration files.</li>
                        <br />
                        <li>Versioned Code - All code that is a good candidate for versioning will be available in your own Bitbucket account. You will have full access to this account as well.</li>
                        <br />
                    </ul>

                    <div>Average Cost - The average cost for 1 server, running 1 website, set up and managed by us, for 24 hours a day, is $1000 per week with monthly costs for hosting/ssl/domain added as needed. Your cost may vary depending on your needs.</div>
                    <br />
                </h5>
                <div class="text-center"><a href="#contact" class="btn btn-danger">Contact us for a Free Quote!</a></div>
            </div>
        </div>

        <div class="index-container-spacer"></div>

        <div class="row" id="information-technology-staffing">
            <div class="col-xs-12">
                <h3>IT Staffing Details</h3>
                <h5 style="line-height:1.5">
                    <div>{{ config('app.name') }} has a vast network of IT professionals. We offer support in the form of professional recruiting.</div>
                    <ul>
                        <br />
                        <li>Local Talent - We will find qualified individuals that are suited to your needs and passionate about helping you achieve your goals within the locality of your place of buisness. </li>
                        <br />
                        <li>Vetting Process - Our applicants are tested upfront, ensuring only the best and brightest for job placement. This removes the hassle of finding dependable, responsible staff for you.</li>
                        <br />
                    </ul>
                    <div>Staff Pricing - The price for all staff is 10% of their yearly salary.</div>
                    <br />
                </h5>
                <div class="text-center"><a href="#contact" class="btn btn-danger">Contact us for a Free Quote!</a></div>
            </div>
        </div>

        <div class="index-container-spacer"></div>
        <br /><br /><br />
    </div>

<?/* CONTACT FORM */?>
	<div id="contact" class="row text-center" style="color:#23527c;font-weight:bold;background-image:url('/images/background_gray.png');padding:10px 0 20px 0;background-size:cover;">
	    <div class="col-xs-12">
	        <h2>Contact Us</h2>
	        <h4>Contact us for questions or a free quote for your project and let's get started.</h4>
	    </div>
	</div>

	<div class="row" style="background-image:url('/images/background_registration.png');background-position:center center;background-size:cover;">
	    <br />
	    <div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
	        <form action="{{ route('support.contact.create') }}" method="post" id="google_captcha_form">
	            @csrf
	            <div class="form-group">
	                <? $full_name = trim(@Auth::user()->fname . ' ' . @Auth::user()->lname); ?>
	                <input placeholder="your full name" type="text" name="full_name" value="<?= old('full_name')??$full_name ?>" class="form-control" />
	            </div>

	            <div class="form-group">
	                <input placeholder="your phone number" type="text" name="phone" value="{{ old('phone') }}" class="form-control" />
	            </div>

	            <div class="form-group">
	                <input placeholder="your email address" type="text" name="email" value="<?= old('email')??@Auth::user()->email ?>" class="form-control" />
	            </div>

	            <div class="form-group">
	                <textarea placeholder="a brief message explaining your project or inquiry" name="message" class="form-control" rows="10">{{ old('message') }}</textarea>
	            </div>

	            <button class="g-recaptcha btn btn-primary pull-right"
					data-sitekey="{{ config('app.google_recaptcha_site_key') }}"
					data-callback='onSubmit'
	                data-badge="bottomleft"><i class="fas fa-paper-plane"></i>&nbsp;Send Email</button>
	            <br /><br /><br />
	        </form>
	    </div>
	</div>
@endsection
