@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h3 class="text-center">Terms and Conditions</h3><br />
            All rules and guidelines must be adhered at all times otherwise loss of membership may occur.
            <br /><br />
            The {{ config('app.name') }} directory, logo and slogans are the property of {{ config('app.name') }}. Reproduction of the {{ config('app.name') }} directory any of its parts in whole or part is strictly prohibited, unless written permission is given by {{ config('app.name') }}.
            <br /><br />
            <h4>NAMING OF OWNER</h4>
            {{ config('app.name') }} is a product and service of {{ config('app.name') }} located in Tampa, Florida. {{ config('app.name') }} shall be known as OWNER and as the owner of {{ config('app.name') }} within the confines of this contract. {{ config('app.name') }} shall also be, responsible for, and, absolved of, all obligations and penalties specifically or un-specifically related to OWNER in the confines of this contract.
            <br /><br />
            <h4>LIABILITIES</h4>
            USER EXPRESSLY WAIVES AND RELEASES OWNER FROM ANY AND ALL LIABILITY FOR INCIDENTAL, SPECIAL AND/OR CONSEQUENTIAL DAMAGES (INCLUDING, WITHOUT LIMITATION, CLAIMS FOR LOST PROFITS OR BUSINESS).
            <br /><br />
            ANY USER OF THIS WEBSITE ASSUMES ALL RESPONSIBILITY AND RISK FOR THE USE OF THIS WEBSITE AND THE INTERNET GENERALLY. OWNER ASSUMES NO RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, RELIABILITY OR USEFULNESS OF ANY INFORMATION (OR OTHER MATERIAL), APPARATUS, OR OTHER PROCESS CONTAINED ON, DISTRIBUTED THROUGH, OR LINKED DOWNLOADED OR ACCESSED FROM THIS WEBSITE.
            <br /><br />
            IN NO EVENT SHALL OWNER OR ITS AFFILIATES BE LIABLE FOR ANY SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER (INCLUDING BUT NOT LIMITED TO LOSS OF USE, DATA, OR PROFITS) ARISING OUT OF OR IN CONNECTION WITH (A) THE USE OR PERFORMANCE OF THIS WEBSITE OR ANY THIRD PARTY SITE (DEFINED BELOW), (B) ANY INFORMATION, MATERIAL, APPARATUS OR OTHER PROCESS CONTAINED ON, DISTRIBUTED THROUGH, OR LINKED, DOWNLOADED OR ACCESSED FROM THIS WEBSITE (INCLUDING WITHOUT LIMITATION, THOSE CONTAINED ON A THIRD PARTY SITE), (C) ANY PRODUCTS OR SERVICES PURCHASED ON OR THROUGH A THIRD PARTY SITE, OR ANY PRODUCTS OR SERVICES PURCHASED OR OBTAINED AS A RESULT OF AN ADVERTISEMENT OR OTHER INFORMATION OR MATERIAL ON OR IN CONNECTION WITH THIS WEBSITE, OR (D) THE INTERNET GENERALLY, OR OTHERWISE RELATING TO THE SUBJECT MATTER OF THIS AGREEMENT; WHETHER BASED ON BREACH OF CONTRACT, NEGLIGENCE OR OTHER TORT, OR OTHER FORM OF ACTION OR LEGAL THEORY, REGARDLESS OF WHETHER OWNER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
            <br /><br />
            OWNER reserves the right to offer, limit or refuse service to any single individual, partnership, corporation or organization for any time length for the violation of any one of the Terms of Service specified or alluded to in this contract.
            <br /><br />
            <h4>SECURITY/NO DISRUPTION</h4>
            You agree that you will comply with any security processes and procedures (such as passwords) specified by OWNER with respect to access to or use of this website. Further, you agree not to access or attempt to access any areas of or through this website which are not intended for general public or private USER access, unless you have been provided with explicit written authorization to do so by OWNER. You agree that you will not disrupt the functioning of this website or otherwise act in a way that interferes with other users' use of this website. You will also in now way try to data mine coding or information in any way to replicate or reproduce {{ config('app.name') }}.
            <br /><br />
            <h4>DATA MINING PROHIBITED</h4>
            USER may not use bots or similar methods or tools to "data mine" or otherwise gather or extract data from this or any of our affiliates websites.
            <br /><br />
            <h4>TRADEMARKS</h4>
            The {{ config('app.name') }} name and logo and all other related product and service names, design marks and slogans are trademarks, service marks or registered trademarks of OWNER or its USERS and may not be used in any manner without the prior written consent of OWNER or its USERS that own any such mark(s). All other trademarks and service marks are trademarks of their respective owners.
            <br /><br />
            <h4>CONSENT TO MONITORING</h4>
            OWNER is under no obligation to monitor the information residing on or transmitted to this website. However, anyone accessing this website agrees that OWNER may monitor the contents of this website periodically to (1) comply with any applicable laws, regulations or other governmental requests; (2) operate this website properly or to protect itself and its USERS. OWNER asks parents or legal guardians to supervise the activities of children at this and any website. OWNER does not knowingly collect any information from minors through this website. OWNER is not liable for any specific or unspecific USER which collects personal information from a minor.
            <br /><br />
            <h4>CANCELLATION POLICY</h4>
            USER is obligated to pay the rest of their subscription time.  They WILL NOT be reimbursed or prorated for the time they choose to not use the website.
            <br /><br />
            <h4>MEMBERSHIP BENEFITS</h4>
            USER is not entitled to receive any benefits exclusively pertaining to a {{ config('app.name') }} USER other than what is found on {{ config('app.name') }}. Any and all other forms of advertisements promoting or promising {{ config('app.name') }} USER benefits in any way related to OWNER or third parties is deemed invalid unless otherwise endorsed or found on {{ config('app.name') }}.
            <br /><br />
            <h4>ARBITRATION/DISPUTE RESOLUTIONS</h4>
            Any and all dispute settlements, hearings or proceeding will be held in the Satellite Beach, county of Brevard in the state of Florida. Not excluded are any and all pre-trial and such procedures affiliated with an arbitration or dispute resolution in any manner. All cases will be heard by the local Arbitration Board or local Court in named area and resulting judgment will be deemed as acceptable by both parties. Both parties waive their right to pursue any further compensation or reduction of penalty beyond the Arbitration Board or local Courts final judgment.
            <br /><br />
            <h4>TERMS OF SERVICE CONTRACT</h4>
            The Terms of Service constitutes the entire agreement of the parties with regard to the subject matter hereof, and supersedes any and all prior oral and/or written agreements regarding the same subject matter hereof. No modification, alteration or addition to these terms shall be enforceable unless expressly accepted in writing by OWNER. USER acknowledges receipt of and agrees to the complete Terms of Service.
            <br /><br />
            Any USER which violates any of the Terms of Service in this contract will be subject to suspension or cancellation of subscription, products and/or services pertaining to OWNER exclusively; or termination of this contract without notice. If USER does not agree to all the Terms of Service in this agreement they must stop using the {{ config('app.name') }} website and or membership immediately.
            <br /><br />
            I the USER understand that if found or thought to be in violation of any {{ config('app.name') }} Terms of Service that OWNER reserves the right to limit or refuse any form of service to any single individual, partnership, corporation or organization for any length of time. I have fully read and acknowledge, understand and agree to abide by all the Terms of Service as found on {{ config('app.name') }}.
        </div>
    </div>
@endsection
