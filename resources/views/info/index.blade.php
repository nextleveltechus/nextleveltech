@extends('common/layout')

@section('content')
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('info.terms') }}" class="btn btn-default btn-block">Terms and Conditions</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <div class="form-group">
            <a href="{{ route('info.privacy') }}" class="btn btn-default btn-block">Privacy Policy</a>
        </div>
    </div>

    <br /><br /><br />
@endsection
