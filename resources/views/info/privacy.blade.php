@extends('common/layout')

@section('content')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h3 class="text-center">Privacy Policy</h4><br/>
            At {{ config('app.name') }}, we value your trust.  We want you to know how we collect, use, share and protect information about you.
            <br/><br/>
            What Information is Collected?
            <br /><br />
            We collect information, including when you:
            <br/><br/>
            1. Create a {{ config('app.name') }} account
            <br/>
            2. Use a social media service, for example, {{ config('app.name') }}'s Facebook page, Twitter page, Blogger, or LinkedIn account
            <br/>
            3. Sign up for e-mails, mobile messages, or social media notifications from {{ config('app.name') }}
            <br/>
            4. Provide us with comments, suggestions, or other input
            <br /><br />
            Types of information we collect include:
            <br/><br/>
            1. Your name
            <br/>
            2. Your mailing address
            <br/>
            3. Your e-mail address
            <br />
            4. Your IP address
            <br /><br />
            Information Collected Online<br/>
            <h4>Cookies</h4>

            We use cookies, web beacons, and other technologies to receive and store certain types of information whenever you interact with our website through your computer or mobile device. This information, which includes the pages you visit on our site, which web address you came from, and the type of browser you are using, helps us customize your website experience and make our marketing messages more relevant. This information also allows us to provide features such as storage of items in your cart between visits.
            <br /><br />
            In order to provide the best guest experience possible, we also use this information for reporting and analysis purposes, such as how you are shopping our website, performance of our marketing efforts, and your response to those marketing efforts.
            <br /><br />
            In order to improve guest online shopping experience, help with fraud identification, and to assist our guest relations representatives in resolving issues guests may experience in completing online purchases, we use tools to monitor certain user experience information; including login information, IP address, data regarding pages visited, specific actions taken on pages visited (e.g. information entered during checkout process), and browser information.
            Public Forums
            <br /><br />
            Any information you submit in a public forum (e.g. a blog, chat room, or social network) can be read, collected, or used by us and others, and could be used to personalize your experience. You are responsible for the information you choose to submit in these instances.
            How is this Information Used?
            Examples of how we use your information include:
            Product and Service Fulfillment
            <br /><br />
            Fulfill and manage purchases, orders, payments, returns/exchanges, or requests for information, or to otherwise serve you
            Internal Operations
            <br /><br />
            Improve the effectiveness of our website, mobile experience, and marketing efforts
            <br /><br />
            Conduct research and analysis, including focus groups and surveys
            <br /><br />
            Perform other business activities as needed, or as described elsewhere in this policy
            Fraud Prevention
            <br /><br />
            To prevent fraudulent transactions, monitor against theft and otherwise protect our guests and our business<br /><br />
            <h4>Legal Compliance</h4>

            To assist law enforcement and respond to subpoenas<br /><br />
            How is this Information Shared?<br /><br />
            We may share information with companies that provide support services to us (such as a printer, e-mail, mobile marketing, or data enhancement provider) or that help us market our products and services. These companies may need information about you in order to perform their functions.
            <br /><br />
            These companies are not authorized to use the information we share with them for any other purpose.
            Legal Requirements<br />
            We may disclose information you provide to us when we believe disclosure is appropriate to comply with the law; to enforce or apply applicable terms and conditions and other agreements; or to protect the rights, property or safety of our company, our guests or others.
            Elsewhere at Your Direction<br /><br />
            At your direction or request, we may share your information. For example, if you create a gift registry or shopping list and elect to make it available to the public, your information will be accessible on our website, mobile or social applications.<br /><br />
            How is this Information Protected?<br />
            <h4>Security Methods</h4>

            We maintain administrative, technical and physical safeguards to protect your information. This includes encrypting your credit and debit card number.
            <br /><br />
            <h4>Email Security</h4>
            "Phishing" is a scam designed to steal your personal information. If you receive an e-mail that looks like it is from us asking you for your personal information, do not respond. We will never request your password, user name, credit card information or other personal information through e-mail.<br /><br />
            <h4>Safe Shopping Guarantee</h4>
            Our security measures are designed to prevent anyone from stealing and using your credit card number. Under the Fair Credit Billing Act, if your credit card is used without your authorization, your liability for those fraudulent charges cannot exceed $50.
            Information About Children<br /><br />
            We recognize the particular importance of protecting privacy where children are involved. We are committed to protecting children's privacy and we comply fully with the Children's Online Privacy Protection Act (COPPA). We do not knowingly collect personally identifiable information from children under the age of 13. If a child under the age of 13 has provided us with personally identifiable information, we ask that a parent or guardian contact us.
        </div>
    </div>
@endsection
