	<br />
	<div class="row text-center">
		<div class="col-xs-12 text-center">
			<h5>
				<a href="{{ route('info.terms') }}">Terms of Service</a>
				&nbsp;&nbsp;&bull;&nbsp;
				<a href="{{ route('info.privacy') }}">Privacy Policy</a>
			</h5>
			<br />
			<h4>&copy; <?=date('Y')?> {{ config('app.name') }}</h4>
			<h6>
				<span class="poweredby">Powered by Next Level Technology</span>
			</h6>
		</div>
	</div>
	<br />
