<li class="dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle main-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{ Auth::user()->fname }} {{ Auth::user()->lname }}<span class="caret"></span></a>

    <ul class="dropdown-menu">
        <?/*{!! server_setup_link() !!}*/?>
        <?/*{!! payment_link() !!}*/?>
        <?/*{!! reports_link() !!}*/?>
        {!! uploads_link() !!}
        {!! account_link() !!}
        {!! logout_link() !!}
    </ul>
</li>
