<?
    $uri = Request::path();
    $catch = [
        ''
        ,'/'
        ,'index'
        ,'/index'
        ,'/index/index'
    ];
?>

<? if(in_array($uri,$catch)): ?>
<li class="dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle main-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('Services') }} <span class="caret"></span></a>

    <ul class="dropdown-menu">
        {!! phone_apps_link() !!}
        {!! server_management_link() !!}
        {!! it_staffing_link() !!}
        <?/*{!! professional_office_cleaning() !!}*/?>
        {!! stock_supplies() !!}
    </ul>
</li>
<? endif; ?>
