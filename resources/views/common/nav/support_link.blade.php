<li class="dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle main-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('Support') }} <span class="caret"></span></a>
    <ul class="dropdown-menu">
        {!! help_block_link() !!}
        {!! contact_link() !!}
        {!! tickets_link() !!}
    </ul>
</li>
