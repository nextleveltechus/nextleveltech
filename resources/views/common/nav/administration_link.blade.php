<li class="dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle main-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('Administration') }} <span class="caret"></span></a>

    <ul class="dropdown-menu">
        {!! manage_users_link() !!}
        {!! manage_tickets_link() !!}
        {!! manage_contacts_link() !!}
        <?/*{!! view_jobs_link() !!}*/?>
        <?/*{!! view_billing_link() !!}*/?>
        {!! support_emails_link() !!}
    </ul>
</li>
