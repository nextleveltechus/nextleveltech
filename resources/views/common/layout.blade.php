<!DOCTYPE html>
<html lang="en">
	<head>
		{!! return_analytics() !!}
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>{{ config('app.name') }}</title>

		<link href="{{ mix('/css/all.css') }}" rel="stylesheet">
		<script src="{{ mix('/js/all.js') }}"></script>

		<link href="/js/jquery-ui-1.12.1.custom/jquery-ui.css" type="text/css" rel="Stylesheet" />
		<link href="/js/jquery-ui-1.12.1.custom/jquery-ui-timepicker-addon.css" type="text/css" rel="Stylesheet" />
		<script src="/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
		<script src="/js/jquery-ui-1.12.1.custom/jquery-ui-timepicker-addon.js"></script>

		<script type="text/javascript" rel="stylesheet">
			$(document).ready(function() {
				$('.datetimepicker').datetimepicker({ format: 'yy-mm-dd', stepMinute: 15, hourMin: 5, hourMax: 20 });
				$('.datetimepicker').prop("autocomplete","off");
				$('.datepicker').datepicker({ format: 'yy-mm-dd' });
				$('.datepicker').prop("autocomplete","off");
			});
		</script>

		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-config" content="/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<meta name="description" itemprop="description" content="" />
		<meta name="keywords" itemprop="keywords" content="" />
		<link rel="canonical" href="<?=$_SERVER['HTTP_HOST']?>" />
	</head>

	<body>
		<div class="mainSite">
			@include('common.header')

			<div class="container-fluid">
				<? new \App\Breadcrumb ?>
				@yield('content')
			</div>

			<div id="siteFooter">
				@include('common.footer')
			</div>
		</div>

		<img src="/images/home.png" id="pageUp" />
	</body>
</html>
