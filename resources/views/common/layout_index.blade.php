<!DOCTYPE html>
<html lang="en">
	<head>
		{!! return_analytics() !!}
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>{{ config('app.name') }}</title>

		<link href="{{ mix('/css/all.css') }}" rel="stylesheet">
		<script src="{{ mix('/js/all.js') }}"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-config" content="/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<meta name="description" itemprop="description" content="" />
		<meta name="keywords" itemprop="keywords" content="" />
		<link rel="canonical" href="<?=$_SERVER['HTTP_HOST']?>" />

		<link href="https://fonts.googleapis.com/css2?family=Permanent+Marker&display=swap" rel="stylesheet">
	</head>

	<body>
		<div class="mainSite">
			@yield('content_head')

			<div class="container-fluid" style="padding:40px 0px 0px 0px;margin:0;">
				@yield('content')
			</div>

			<div id="siteFooter">
				@include('common.footer')
			</div>
		</div>

		<img src="/images/home.png" id="pageUp" />
	</body>
</html>
