{{ website_notice() }}
{!! showHelpBlocks() !!}
<nav class="navbar navbar-default navbar-main">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
        data-toggle="collapse"
        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        	<span class="sr-only">Toggle navigation</span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
        </button>
        <a class="navbar-brand main-link hidden-xs" href="{{ route('index') }}">
            {{ config('app.name') }}
        </a>
        <a class="navbar-brand main-link hidden-sm hidden-md hidden-lg hidden-xl" href="{{ route('index') }}">
            {{ config('app.short_name') }}
        </a>
    </div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
            {!! administration_link() !!}
            {!! services_link() !!}
            {!! support_link() !!}
            {!! logged_in_link() !!}
            {!! login_link() !!}
        </ul>
    </div>
</nav>

{!! siteErrors($errors->all()) !!}
