<form action="/admin/ticketresponses/{{ $id }}" method="post">
    <input type="hidden" name="_method" value="delete" />
    @csrf
    <div class="col-xs-6 text-right">
        <div class="form-group">
            <a href="" id="{{ $id }}" class="btn btn-default editResponse">Edit</a>
            <input type="hidden" name="id" value="{{ $id }}" />
            <input type="submit" value="Delete" class="btn btn-default" />
        </div>
    </div>
</form>
