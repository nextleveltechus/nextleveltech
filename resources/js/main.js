import $ from 'jquery';
window.$ = window.jQuery = $;
window.Popper = require('popper.js');

$(document).ready(function() {
	// autofill zipcode field
	$("#zipcode").on("input",function(){
		if($('#zipcode').val().length != 5)
			return;

		$.ajax({
			url: "/api/common/returnCity?zipcode="+$('#zipcode').val()
		}).done(function( data ) {
			$('#city').val(data);
		});

		$.ajax({
			url: "/api/common/returnState?zipcode="+$('#zipcode').val()
		}).done(function( data ) {
			$('#state').val(data);
		});

		$.ajax({
			url: "/api/common/returnCounty?zipcode="+$('#zipcode').val()
		}).done(function( data ) {
			$('#county').val(data);
		});
    });

	$("#siteErrors").animate({ opacity: 'toggle', height: 'toggle' });
	$("#siteWarnings").animate({ opacity: 'toggle', height: 'toggle' });
	$("#siteNotices").animate({ opacity: 'toggle', height: 'toggle' });

	if ($(this).scrollTop() > 50)
		$('#pageUp').fadeIn();

	$('#pageUp').click(function(){
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('#pageUp').fadeIn();
		} else {
			$('#pageUp').fadeOut();
		}
	});

	// only use this
	$('.numPerPage').change(function() {
		this.form.submit();
	});

	$('.showOnly').change(function() {
		this.form.submit();
	});

	$("#toggle-help-block").on('click',function(event) {
		event.preventDefault();

		$(".help-block").slideToggle();

		$.get( "/api/alerts/toggleHelpBlocks")
		.done(function( data ) {
			if(data == 'blue'){
				$(".headerIcon").removeClass("gray").addClass("blue").blur();
			} else {
				$(".headerIcon").removeClass("blue").addClass("gray").blur();
			}
		});
	});

	$('#btn_new_upload').on("click",function(){
		$("#form_upload").animate({ opacity: 'toggle', height: 'toggle' });
	});

	$('#btn_close').on("click",function(){
		$("#form_upload").animate({ opacity: 'toggle', height: 'toggle' });
	});
});
