<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname'
        ,'lname'
        ,'company'
        ,'email'
        ,'phone'
        ,'street1'
        ,'street2'
        ,'city'
        ,'state'
        ,'zipCode'
        ,'notes'
    ];
}
