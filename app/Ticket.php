<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    var $title = NULL;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
        ,'description'
        ,'type'
        ,'status'
        ,'priority'
        ,'startDate'
        ,'endDate'
        ,'estimatedTime'
        ,'percentComplete'
        ,'userId'
        ,'assigneeId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    const STATUS = array(
		"NEW" => "New"
		,"DESIGN" => "Design"
		,"IN PROGRESS" => "In Progress"
		,"FEEDBACK" => "Feedback"
		,"QUALITY ASSURANCE" => "Quality Assurance"
		,"QUALITY ASSURANCE PASSED" => "Quality Assurance Passed"
		,"RELEASED" => "Released"
		,"CLEANUP" => "Cleanup"
		,"RESOLVED" => "Resolved"
		,"ON HOLD" => "On Hold"
		,"CLOSED" => "Closed"
		);

	const PRIORITY = array(
		"LOW" => "Low"
		,"NORMAL" => "Normal"
		,"HIGH" => "High"
		,"URGENT" => "Urgent"
		,"LATE" => "Late"
		,"IMMEDIATE" => "Immediate"
		);

	const TYPE =  array(
		"BUG" => "Bug"
		,"FEATURE" => "Feature"
		);
}
