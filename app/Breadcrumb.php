<?php

namespace App;

class Breadcrumb
{
    public function __construct()
    {
        $pieces = explode("/",\Request::path());

        if(empty($pieces))
            return;

print <<<HTML
        <div class="row hidden-xs">
            <div class="col-xs-12">
                <a href="/">home</a>
HTML;

        if(@$pieces[0] == 'admin'){
            if(empty($pieces[1])){
print <<<HTML
                &bull; administration
HTML;
            } else {
print <<<HTML
                &bull; <a href="/admin">administration</a>
HTML;
            }
        }


        //
        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'tickets'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; manage tickets
HTML;
            } else {
print <<<HTML
                &bull; <a href="/admin/tickets/index">manage tickets</a>
HTML;
            }
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'tickets'
            && @$pieces[2] == 'create'){
print <<<HTML
                &bull; new ticket
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'tickets'
            && @$pieces[3] == 'edit'){
print <<<HTML
                &bull; edit ticket
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'tickets'
            && @$pieces[2] == 'searchQuick'){
print <<<HTML
                &bull; search
HTML;
        }


        //
        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'contacts'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; manage contacts
HTML;
            } else {
print <<<HTML
                &bull; <a href="/admin/contacts/index">manage contacts</a>
HTML;
            }
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'contacts'
            && @$pieces[2] == 'create'){
print <<<HTML
                &bull; new contacts
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'contacts'
            && @$pieces[3] == 'edit'){
print <<<HTML
                &bull; edit contacts
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'contacts'
            && @$pieces[2] == 'searchQuick'){
print <<<HTML
                &bull; search
HTML;
        }


        //
        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'users'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; manage users
HTML;
            } else {
print <<<HTML
                &bull; <a href="/admin/users/index">manage users</a>
HTML;
            }
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'users'
            && @$pieces[2] == 'add'){
print <<<HTML
                &bull; new user
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'users'
            && @$pieces[2] == 'edit'){
print <<<HTML
                &bull; edit user
HTML;
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'users'
            && @$pieces[2] == 'searchQuick'){
print <<<HTML
                &bull; search
HTML;
        }


        //
        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'emails'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; support emails
HTML;
            } else {
print <<<HTML
                &bull; <a href="/admin/emails/index">support emails</a>
HTML;
            }
        }

        if(@$pieces[0] == 'admin'
            && @$pieces[1] == 'emails'
            && @$pieces[2] == 'show'){
print <<<HTML
                &bull; details
HTML;
        }


        //
        if(@$pieces[0] == 'account'){
            if(empty($pieces[1])){
print <<<HTML
                &bull; account
HTML;
            } else {
print <<<HTML
                &bull; <a href="/account">account</a>
HTML;
            }
        }


        //
        if(@$pieces[0] == 'login'){
            if(empty($pieces[1])){
print <<<HTML
    &bull; login
HTML;
            } else {
print <<<HTML
    &bull; <a href="/login">login</a>
HTML;
            }
        }

        if(@$pieces[0] == 'auth'
            && @$pieces[1] == 'login'
            && @$pieces[2] == 'forgot_username'){
print <<<HTML
            &bull; <a href="/login">login</a>
            &bull; forgot username
HTML;
        }

        if(@$pieces[0] == 'auth'
            && @$pieces[1] == 'login'
            && @$pieces[2] == 'forgot_username_success'){
print <<<HTML
            &bull; <a href="/login">login</a>
            &bull; forgot username
HTML;
        }

        if(@$pieces[0] == 'auth'
            && @$pieces[1] == 'login'
            && @$pieces[2] == 'lost_password'){
print <<<HTML
            &bull; <a href="/login">login</a>
            &bull; lost password
HTML;
        }

        if(@$pieces[0] == 'auth'
            && @$pieces[1] == 'login'
            && @$pieces[2] == 'lost_password_success'){
print <<<HTML
            &bull; <a href="/login">login</a>
            &bull; lost password
HTML;
        }


        //
        if(@$pieces[0] == 'info'){
            if(empty($pieces[1])){
print <<<HTML
                &bull; information
HTML;
            } else {
print <<<HTML
                &bull; <a href="/info">information</a>
HTML;
            }
        }

        if(@$pieces[0] == 'info'){
            if(@$pieces[1] == 'privacy'){
print <<<HTML
                &bull; privacy policy
HTML;
            }
        }

        if(@$pieces[0] == 'info'){
            if(@$pieces[1] == 'terms'){
print <<<HTML
                &bull; terms and conditions
HTML;
            }
        }


        if(@$pieces[0] == 'support'){
            if(empty($pieces[1])){
print <<<HTML
                &bull; support
HTML;
            } else {
print <<<HTML
                &bull; <a href="/support">support</a>
HTML;
            }
        }


        //
        if(@$pieces[0] == 'support'
            && @$pieces[1] == 'contact'){
print <<<HTML
                &bull; contact
HTML;
        }


        //
        if(@$pieces[0] == 'support'
            && @$pieces[1] == 'tickets'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; tickets
HTML;
            } else {
print <<<HTML
                &bull; <a href="/support/tickets/index">tickets</a>
HTML;
            }
        }

        if(@$pieces[0] == 'support'
            && @$pieces[1] == 'tickets'
            && @$pieces[2] == 'create'){
print <<<HTML
                &bull; new ticket
HTML;
        }

        if(@$pieces[0] == 'support'
            && @$pieces[1] == 'tickets'
            && @$pieces[3] == 'edit'){
print <<<HTML
                &bull; edit ticket
HTML;
        }

        if(@$pieces[0] == 'support'
            && @$pieces[1] == 'tickets'
            && @$pieces[2] == 'searchQuick'){
print <<<HTML
                &bull; search
HTML;
        }






        //
        if(@$pieces[0] == 'contact'
            && @$pieces[1] == 'create'){
print <<<HTML
                &bull; contact
HTML;
        }




        //
        if(@$pieces[0] == 'account'
            && @$pieces[1] == 'uploads'){
            if(@$pieces[2] == 'index'){
print <<<HTML
                &bull; uploads
HTML;
            } else {
print <<<HTML
                &bull; <a href="/account/uploads/index">uploads</a>
HTML;
            }
        }

        //
        if(@$pieces[0] == 'account'
            && @$pieces[1] == 'uploads'
            && @$pieces[2] == 'searchQuick'){
print <<<HTML
                &bull; search
HTML;
        }



print <<<HTML
                <br /><br />
            </div>
        </div>
HTML;

        return;
    }
}
