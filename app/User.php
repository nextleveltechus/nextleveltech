<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname'
        ,'lname'
        ,'email'
        ,'timezone'
        ,'username'
        ,'password'
        ,'status'
        ,'type'
        ,'password_token_timeout'
        ,'password_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    const STATUS = array(
		"PENDING" => "Pending"
		,"ACTIVE" => "Active"
		,"SUSPENDED" => "Suspended"
		,"CANCELLED" => "Cancelled"
		);

	const TYPE =  array(
		"SYSTEM ADMINISTRATOR" => "System Administrator"
		,"USER" => "User"
		);

    const TIMEZONE = array(
		"Eastern" => "America/New_York"
		,"Central" => "America/Chicago"
		,"Mountain" => "America/Denver"
		,"Mountain no DST" => "America/Phoenix"
		,"Pacific" => "America/Los_Angeles"
		,"Alaska" => "America/Anchorage"
		,"Hawaii" => "America/Adak"
		,"Hawaii no DST" => "Pacific/Honolulu"
		);


    public static function system_admins()
    {
        $arr = array();

        $users = \App\User::orderBy('fname')
            ->where('type',\App\User::TYPE['SYSTEM ADMINISTRATOR'])
            ->get();

        if(!$users->isEmpty())
            foreach($users as $k => $v)
                $arr[$v['id']] = "{$v['fname']} {$v['lname']}";

        return $arr;
    }

    public static function system_admins_array()
    {
        $arr = array();

        $users = \App\User::orderBy('fname')
            ->where('type',\App\User::TYPE['SYSTEM ADMINISTRATOR'])
            ->get();

        if(!$users->isEmpty())
            foreach($users as $k => $v)
                $arr[$v->id] = $v->id;

        return $arr;
    }


    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
    * Overrides the method to ignore the remember token.
    */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute){
            parent::setAttribute($key, $value);
        }
    }


    //
    public static function refresh_session()
    {
        if(!\Auth::check())
            return;

        $user = \App\User::find(\Auth::user()->id);

        return \Auth::login($user);
    }
}
