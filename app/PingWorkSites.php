<?php

namespace App;

class PingWorkSites
{
    var $sites_down = [];
    var $sites = [
        'php7' => 'https://admin.mytrueserve.com/server_check'
        ,'php5' => 'https://cms.mytrueserve.com/server_check.php'
    ];
//'userve' => 'https://eozproperties.com/server_check'
    //
    public function __construct()
    {

    }


    //
    public function handle()
    {
        $this->_ping_websites();
    }


    //
    private function _ping_websites()
    {
        if(empty($this->sites))
            return;

        foreach($this->sites as $k => $v)
            $this->_curl($k, $v);

        $this->_send_text_notifications();

        exit;
    }


    //
    private function _curl($name = null, $url = null)
    {
        if(empty($name) || empty($url))
            return;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);

        $data = curl_exec($curl);
//print $data;
        if(curl_errno($curl) || $data != 'up')
            $this->sites_down[] = $name;

        curl_close($curl);
    }


    //
    private function _send_text_notifications()
    {
        if(empty($this->sites_down))
            return;

        $subject = '';
        $message = implode(', ',$this->sites_down) . ' might be down';
        $headers = 'FROM: support@nextleveltech.us';

        mail('5616747063@vtext.com',$subject,$message,$headers);

        return;
    }
}
