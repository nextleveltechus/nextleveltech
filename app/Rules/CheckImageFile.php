<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckImageFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $allowed_extensions = explode('|',config('app.image_extensions_allowed'));

        $info = new \SplFileInfo($_FILES['userfile']['name']);
        $extension = strtolower($info->getExtension());

        if(!in_array($extension,$allowed_extensions))
            return FALSE;

        if($_FILES['userfile']['name'] <= config('app.file_max_upload_size'))
            return FALSE;

        return TRUE;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $allowed_extensions = implode(', ',explode('|',config('app.image_extensions_allowed')));
        $bytes = config('app.file_max_upload_size');

        return "The upload file must have the extension of {$allowed_extensions} and be less than {$bytes} bytes.";
    }
}
