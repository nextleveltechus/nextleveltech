<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'basepath'
        ,'title'
        ,'raw_name'
        ,'file_ext'
        ,'file_size'
        ,'image_width'
        ,'image_height'
        ,'image_size_str'
        ,'mime'
        ,'userId'
        ,'instanceType'
        ,'instanceId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    const INSTANCE_TYPE = array(
    		'TICKET' => 'TICKET'
            ,'USER' => 'USER'
		);


    //
    public static function return_type_array()
    {
        $pieces = explode('|', config('app.file_extensions_allowed'));

        $arr = array();

        if(!empty($pieces))
            foreach(array_sort($pieces) as $k => $v)
                $arr[$v] = $v;

        return $arr;
    }
}
