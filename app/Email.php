<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //
    protected $fillable = [
        'to'
        ,'from'
        ,'name'
        ,'phone'
        ,'message'
        ,'auth'
        ,'request'
        ,'server'
        ,'cookie'
        ,'session'
        ,'deleted'
    ];

    const DELETED = array(
		"TRUE"
		,"FALSE"
		);


}
