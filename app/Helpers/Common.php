<?php
    //
    function license_image()
    {
        if(!\Auth::check())
            return;

        $license = \App\File::where('instanceId',\Auth::user()->id)
            ->where('instanceType',\App\File::INSTANCE_TYPE['LICENSE'])
            ->whereNull('deleted_at')
            ->first();

        if(empty($license))
            return;

print <<<HTML
        <div class="row text-center">
            <div class="col-xs-12">
                Server License <a data-toggle="modal" data-target="#img">View</a> - <a data-toggle="modal" data-target="#delete">Delete</a>
            </div>
        </div>

        <div class="modal fade text-center" tabindex="-1" role="dialog" id="img">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <img src="/api/files/license" />
                    </div>
                </div>
            </div>
        </div>
HTML;
    }


    //
    function format_for_picker( $date = NULL, $no_time = FALSE )
    {
        if(empty($date))
            return;

        return ($no_time) ?  date("m/d/Y",strtotime($date)) : date("m/d/Y H:i:s",strtotime($date));
    }

    //
    function convert_date_db( $date = NULL )
    {
        return (!empty($date)) ? date("Y-m-d H:i:s",strtotime($date)) : NULL;
    }

    //
    function isVisibleText( $str = NULL )
    {
        return ($str == strtoupper(\App\TicketResponse::ISVISIBLE['VISIBLE'])) ? 'Visible to all' : 'Hidden from users';
    }

    //
    function controlLinks( $ticketResponse = array() )
	{
		if(empty($ticketResponse))
			return;

		if($ticketResponse['userId'] != Auth::user()->id)
			return;

        print view('helpers.common.controlLinks',$ticketResponse);
	}


    //
    function controlLinksMember( $ticketResponse = array() )
	{
		if(empty($ticketResponse))
			return;

		if($ticketResponse['userId'] != Auth::user()->id)
			return;

        print view('helpers.common.controlLinksMember',$ticketResponse);
	}

    //
    function form_registration( $type = NULL )
    {
        if(!\Auth::check())
            if(in_array($type,\App\User::TYPE) && $type != 'System Administrator'){
                $data['type'] = $type;

                return view('auth.form_registration', $data);
            }
    }

    function return_analytics()
    {
        if(config('app.env') == 'production')
return <<<HTML
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132733691-2"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-132733691-2');
            </script>
HTML;
    }


    //
    function format_for_select( $arr = [] )
    {
        if(empty($arr))
            return $arr;

        foreach($arr as $k => $v)
            $arr[$k] = ucwords(strtolower($v));

        return $arr;
    }


    //
    function titleize_underscores( $str = NULL )
    {
        if(empty($str))
            return;

        $str = str_replace('_',' ',$str);
		return ucwords($str);
    }


    //
    function currency( $currency = NULL )
    {
        if(empty($currency))
            return;

        print ($currency >= 0) ? '$' . number_format($currency,2) : '<span class="negative">($' . number_format(abs($currency),2) . ')</span>';

        return;
    }


    //
    function display_location( $arr = array() )
    {
        if(!empty($arr['city'])
            && !empty($arr['state']))
                return "{$arr['city']}, {$arr['state']} {$arr['zipCode']}";

        return "{$arr['city']} {$arr['state']} {$arr['zipCode']}";
    }

    //
    function display_location_no_zip( $arr = array() )
    {
        if(!empty($arr['city'])
            && !empty($arr['state']))
                return "{$arr['city']}, {$arr['state']}";

        return "{$arr['city']} {$arr['state']}";
    }

    //
    function titleize( $str )
	{
		$str = preg_split('#([A-Z][^A-Z]*)#', $str, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$str = implode($str,' ');
		return ucwords($str);
	}


    // used to list serialized data in tickets updates
    function listArray( $serialized )
	{
		if(empty($serialized))
			return;

		$str = '<div class="row"><div class="col-xs-12"><em>';
		foreach(unserialize($serialized) as $k => $v)
			$str .= "<div>{$v}</div>";
		$str .= '</em></div></div><br />';

		print $str;
	}


    //
    function website_notice()
    {
print <<<HTML
            <div class="websiteNotice">
HTML;
                print config('app.website_notice');
PRINT <<<HTML
            </div>
HTML;
    }


    function displayFromStringDate($str = null, $convert = TRUE)
    {
        if(empty($str))
            return;

        $datetime = new DateTime($str);
        (\Auth::check() && $convert) ? $datetime->setTimezone(new DateTimeZone(\Auth::user()->timezone)) : NULL;

        return $datetime->format('n/j/Y');
    }


    function displayFromStringDateTime($str = null, $convert = TRUE)
    {
        if(empty($str))
            return;

        $datetime = new DateTime($str);
        (\Auth::check() && $convert) ? $datetime->setTimezone(new DateTimeZone(\Auth::user()->timezone)) : NULL;

        return $datetime->format('n/j/Y g:ia');
    }


    function displayFromStringDateTimeRaw($str = null, $convert = TRUE)
    {
        if(empty($str))
            return;

        $datetime = new DateTime($str);
        (\Auth::check() && $convert) ? $datetime->setTimezone(new DateTimeZone(\Auth::user()->timezone)) : NULL;

        return $datetime->format('Y-m-d H:i:s');
    }


    function displayFormattedDate($unix_timestamp = null, $convert = TRUE)
    {
        if(empty($unix_timestamp))
            return;

        $str = date('Y-m-d H:i:s', $unix_timestamp);

        $datetime = new DateTime($str);
        (\Auth::check() && $convert) ? $datetime->setTimezone(new DateTimeZone(\Auth::user()->timezone)) : NULL;

        return $datetime->format('n/j/Y');
    }

    function displayFormattedDateTime($unix_timestamp = null, $convert = TRUE)
    {
        if(empty($unix_timestamp))
            return;

        $str = date('Y-m-d H:i:s', $unix_timestamp);

        $datetime = new DateTime($str);
        (\Auth::check() && $convert) ? $datetime->setTimezone(new DateTimeZone(\Auth::user()->timezone)) : NULL;

        return $datetime->format('n/j/Y g:ia');
    }


    /* return http or https depending on what your're on */
    function displayProtocol()
    {
        return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
    }

    /* convert slash notation uri into a manageable array */
    function querystring_to_array( $queryString = NULL )
    {
        $res = array();

        if(empty($queryString))
            return $res;

        $pieces = explode('/',$queryString);

        if(empty($pieces) || sizeof($pieces) % 2 != 0)
            return $res;

        foreach($pieces as $k => $v)
            if($k % 2 == 0)
                $res[$v] = $pieces[$k+1];

        return $res;
    }


    /* get a single parameter from request */
    function get_request_param( $label = NULL, $request = NULL )
    {
        if(empty($label) || empty($request))
            return;

        $pieces = explode('/',$request);

        foreach($pieces as $k => $v)
            if(strtolower($v) == strtolower($label))
                return @$pieces[$k+1];

        return;
    }


    /* get a single parameter from slash notation uri */
    function get_param( $label = NULL, $uri = NULL )
    {
        if(empty($label) || empty($uri))
            return;

        foreach(querystring_to_array($uri) as $k => $v)
            if(strtolower($k) == strtolower($label))
                return $v;

        return;
    }


    //**
    function siteErrors( $errors )
    {
        $html = '';

        if(!empty(session()->get('error')))
            $errors[] = session()->pull('error');

        if(!empty($errors)){
$html = <<<HTML
    <div id="siteErrors">
        <table align="center">
            <tr>
                <td width="1" valign="top"><span class="glyphicon glyphicon-remove-circle"></span></td>
                <td>
HTML;

            foreach($errors as $k => $v){
$html .= <<<HTML
    <div>&nbsp;{$v}</div>
HTML;
            }

$html .= <<<HTML
                </td>
            </tr>
        </table>
    </div>
HTML;
        }

        if(!empty(session()->get('notice')))
            $notices[] = session()->pull('notice');

            if(!empty($notices)){
$html = <<<HTML
    <div id="siteNotices">
        <table align="center">
            <tr>
                <td width="1" valign="top"><span class="glyphicon glyphicon-ok-circle"></span></td>
                <td>
HTML;

            foreach($notices as $k => $v){
$html .= <<<HTML
    <div>&nbsp;{$v}</div>
HTML;
            }

$html .= <<<HTML
    </td></tr></table></div>
HTML;
        }

        return $html;
    }


    //**
    function showHelpBlocks()
    {
        return (!\Cookie::get('showHelpBlocks')) ? "<style>.help-block { display:none; }</style>" : NULL;
    }
