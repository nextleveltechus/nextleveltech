<?php
    //
    function services_link()
    {
        return view('common.nav.services_link');
    }

    //
    function server_management_link()
    {
        return view('common.nav.server_management_link');
    }

    //
    function it_staffing_link()
    {
        return view('common.nav.it_staffing_link');
    }

    //
    function phone_apps_link()
    {
        return view('common.nav.phone_apps_link');
    }

    //
    function professional_office_cleaning()
    {
        return view('common.nav.professional_office_cleaning');
    }

    //
    function stock_supplies()
    {
        return view('common.nav.stock_supplies');
    }


    //
    function server_setup_link()
    {
        if(\Auth::check())
            return view('common.nav.server_setup_link');

        return;
    }

    //
    function search_jobs_link()
    {
        if(\Auth::check())
            return view('common.nav.search_jobs_link');

        return;
    }

    //
    function view_jobs_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.view_jobs_link');

        return;
    }

    //
    function view_billing_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.view_billing_link');

        return;
    }

    //
    function reports_link()
    {
        return (\Auth::check()) ? view('common.nav.reports_link') : NULL;
    }

    //
    function balance()
    {
        if(\Auth::check())
            return view('common.nav.balance');

        return;
    }

    //
    function payment_link()
    {
        if(\Auth::check())
            return view('common.nav.payment_link');

        return;
    }

    //
    function uploads_link()
    {
        return (\Auth::check()) ? view('common.nav.uploads_link') : NULL;
    }

    //
    function job_link()
    {
        if(\Auth::check())
            return view('common.nav.job_link');

        return;
    }

    //
    function support_link()
    {
        return view('common.nav.support_link');
    }

    //
    function help_block_link()
    {
        $data['help_blocks'] = (!\Cookie::get('showHelpBlocks')) ? " gray" : " blue";

        return view('common.nav.help_block_link', $data);
    }

    //
    function contact_link()
    {
        return view('common.nav.contact_link');
    }

    //
    function login_link()
    {
        return (!\Auth::check()) ? view('common.nav.login_link') : NULL;
    }

    //
    function logged_in_link()
    {
        return (\Auth::check()) ? view('common.nav.logged_in_link') : NULL;
    }

    //
    function account_link()
    {
        return (\Auth::check()) ? view('common.nav.account_link') : NULL;
    }

    //
    function logout_link()
    {
        return (\Auth::check()) ? view('common.nav.logout_link') : NULL;
    }

    //
    function tickets_link()
    {
        return (\Auth::check()) ? view('common.nav.tickets_link') : NULL;
    }

    //
    function administration_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.administration_link');

        return;
    }

    //
    function manage_users_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.manage_users_link');

        return;
    }

    //
    function manage_tickets_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.manage_tickets_link');

        return;
    }


    //
    function manage_contacts_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.manage_contacts_link');

        return;
    }

    //
    function support_emails_link()
    {
        if(\Auth::check())
            if(\Auth::user()->type == strtoupper(\App\User::TYPE['SYSTEM ADMINISTRATOR']))
                return view('common.nav.support_emails_link');

        return;
    }
