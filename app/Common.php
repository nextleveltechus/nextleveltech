<?php

namespace App;

class Common
{
    const NUMPERPAGE = array(
		5 => 5
		,10 => 10
		,20 => 20
		,40 => 40
		,80 => 80
		,160 => 160
	);

	const PERCENTS = array(
		0 => '0%'
		,10 => '10%'
		,20 => '20%'
		,30 => '30%'
		,40 => '40%'
		,50 => '50%'
		,60 => '60%'
		,70 => '70%'
		,80 => '80%'
		,90 => '90%'
		,100 => '100%'
	);

	const MONTHS = array(
		1 => 'January (1)'
		,2 => 'February (2)'
		,3 => 'March (3)'
		,4 => 'April (4)'
		,5 => 'May (5)'
		,6 => 'June (6)'
		,7 => 'July (7)'
		,8 => 'August (8)'
		,9 => 'September (9)'
		,10 => 'October (10)'
		,11 => 'November (11)'
		,12 => 'December (12)'
	);


    //
    public static function create_dir_from_id( $base = NULL, $fileId = null )
    {
        $folders = str_split(strrev((int)$fileId));

		foreach($folders as $k => $v){
			$base .= "/{$v}";
			@mkdir($base);
		}

		return "{$base}/";
    }


    //
    public static function select_array_from_collection( $collection = array(), $key = NULL, $value = NULL)
    {
        if(empty($collection))
            return array();

        foreach($collection as $k => $v)
            $arr[$v->$key] = ($value == 'name') ? "{$v->fname} {$v->lname}" : $v->$value;

        return $arr;
    }
}
