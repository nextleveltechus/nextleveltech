<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketResponse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'response'
        ,'isVisible'
        ,'userId'
        ,'updates'
        ,'ticketId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    const ISVISIBLE = array(
		"VISIBLE" => "Visible"
		,"HIDDEN" => "Hidden"
		);
}
