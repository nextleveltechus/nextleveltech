<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use \Auth;

class TicketResponsesController extends Controller
{
    var $rules = array();
    var $req = array();

    //
    public function __construct()
    {
        $this->requireLoggedIn();

        $this->rules = array(
            'response' => 'nullable'
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // form validation
        $request->validate($this->rules);

        $input = $request->except(['_method','_token','fileTitle','userId']);
        $input['userId'] = Auth::user()->id;

        \App\TicketResponse::find($id)
            ->update($input);

        $response = \App\TicketResponse::find($id);

        \App\Ticket::find($response->ticketId)
            ->();

        $response = \App\TicketResponse::find($id);

        session()->flash('notice','The ticket response has been updated.');
        return redirect()->route('support.tickets.edit',$response->ticketId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = \App\TicketResponse::find($id);
        $updates = unserialize($response->updates);

        \App\Ticket::find($response->ticketId)
            ->touch();

        if(empty($updates))
            \App\TicketResponse::destroy($id);
        else
            \App\TicketResponse::find($id)
                ->update(['response' => '']);

        session()->flash('notice','The ticket response has been deleted.');
        return redirect()->route('support.tickets.edit',$response->ticketId);
    }
}
