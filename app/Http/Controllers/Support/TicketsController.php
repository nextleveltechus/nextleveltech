<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use \Auth;

class TicketsController extends Controller
{
    var $rules = array();
    var $req = array();

    //
    public function __construct()
    {
        $this->requireLoggedIn();

        $this->rules = array(
            'title' => 'required|max:100'
            ,'description' => 'required'
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('numPerPage')??config('app.default_num_per_page');
        $data['ticketsShowOnly'] = \Cookie::get('ticketsShowOnly');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $in = (empty($data['ticketsShowOnly'])) ? array("NEW" => "New","DESIGN" => "Design","IN PROGRESS" => "In Progress","FEEDBACK" => "Feedback","QUALITY ASSURANCE" => "Quality Assurance","QUALITY ASSURANCE PASSED" => "Quality Assurance Passed","RELEASED" => "Released","CLEANUP" => "Cleanup") : array($data['ticketsShowOnly']);

        $data['tickets'] = \App\Ticket::orderBy($sort,$order)
            ->where('userId',Auth::user()->id)
            ->whereIn('tickets.status',$in)
            ->paginate($data['numPerPage']);

        return view('support.tickets.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('support.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($_FILES['userfile']['error']))
            $this->rules['fileTitle'] = 'required|max:100';

        // form validation
        $request->validate($this->rules);

        $input = $request->all();
        $input['startDate'] = 0;
        $input['endDate'] = 0;
        $input['percentComplete'] = 0;
        $input['assigneeId'] = 1;
        $input['userId'] = Auth::user()->id;
        $input['type'] = strtoupper(\App\Ticket::TYPE['BUG']);
        $input['priority'] = strtoupper(\App\Ticket::PRIORITY['HIGH']);
        $input['status'] = strtoupper(\App\Ticket::STATUS['NEW']);

        \App\Ticket::create($input, $request->input('fileTitle'));

        session()->flash('notice','The ticket has been created.');
        return redirect()->route('support.tickets.index');
    }

    private function _save_file( $ticket = array(), $fileTitle = null )
    {
        if(!empty($_FILES['userfile']['error']))
            return;

        $info = new \SplFileInfo($_FILES['userfile']['name']);
        $extension = strtolower($info->getExtension());
        $filename = strtolower(substr(sha1(time()),0,20));

        $size = getimagesize($_FILES['userfile']['tmp_name']);

        $files['basepath'] = config('app.file_basepath');
        $files['title'] = $fileTitle;
        $files['raw_name'] = $filename;
        $files['file_ext'] = $extension;
        $files['file_size'] = $_FILES['userfile']['size'];
        $files['image_width'] = @$size[0];
        $files['image_height'] = @$size[1];
        $files['image_size_str'] = @$size[3];
        $files['mime'] = @$size['mime'];
        $files['userId'] = Auth::user()->id;
        $files['instanceType'] = \App\File::INSTANCE_TYPE['TICKET'];
        $files['instanceId'] = $ticket['id'];

        $file = \App\File::create($files);

        $directory = \App\Common::create_dir_from_id($files['basepath'],$file->id);
        $new_file = "{$directory}/{$files['raw_name']}.{$files['file_ext']}";

		// move file
		copy($_FILES['userfile']['tmp_name'],$new_file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ticket'] = \App\Ticket::where('tickets.id',$id)
            ->select(['tickets.*','u.fname','u.lname','a.fname as assigneefname','a.lname as assigneelname'])
            ->join('users as u','tickets.userId','u.id')
            ->join('users as a','tickets.assigneeId','a.id')
            ->first();

        $data['ticketResponses'] = \App\TicketResponse::orderBy('ticket_responses.created_at','DESC')
            ->select('users.*','ticket_responses.*')
            ->where('ticketId',$id)
            ->where('isVisible',strtoupper(\App\TicketResponse::ISVISIBLE['VISIBLE']))
            ->join('users','userId','users.id')
            ->get();

        $data['files'] = \App\File::orderBy('created_at','DESC')
            ->where('instanceId',$id)
            ->where('instanceType',\App\File::INSTANCE_TYPE['TICKET'])
            ->get();

        return view('support.tickets.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(empty($_FILES['userfile']['error']))
            $this->rules['fileTitle'] = 'required|max:100';

        // form validation
        $request->validate($this->rules);

        $input = $request->except(['_method','_token','fileTitle']);

        $ticket = \App\Ticket::find($id);

        \App\Ticket::find($id)
            ->update($input);

        \App\Ticket::find($id)
            ->touch();

        $updates = array();
        $skipFields = array('description','response','fileTitle','submit','userfile');
		foreach($input as $k => $v){
			if(!in_array($k,$skipFields) && $_POST[$k] != $ticket[$k]){
				$first = ($ticket[$k]) ? ucwords(strtolower($ticket[$k])) : 'Null';
				$second = ($_POST[$k]) ? ucwords(strtolower($_POST[$k])) : 'Null';

                if($first != $second)
		            array_push($updates,titleize($k) . " changed from {$first} to {$second}.");
			}

			if($k == 'description' && $input[$k] != $ticket[$k])
				array_push($updates,"The description has been changed.");
		}

        if(empty($_FILES['userfile']['error']))
            array_push($updates,"The file {$request->input('fileTitle')} was uploaded.");

        $input = $request->except(['_method','_token']);
		$input['updates'] = serialize($updates);
        $input['userId'] = Auth::user()->id;
        $input['ticketId'] = $id;
        $input['isVisible'] = strtoupper(\App\TicketResponse::ISVISIBLE['VISIBLE']);
        \App\TicketResponse::create($input);

        $this->_save_file(['id' => $id, 'title' => $request->input('fileTitle')]);

        session()->flash('notice','The ticket has been updated.');
        return redirect()->route('support.tickets.edit',$id);
    }


    //
    public function searchQuick(Request $request)
    {
        $data['search'] = $request->input('search');

        $data['tickets'] = collect();

        if(!empty($data['search']))
            $data['tickets'] = \App\Ticket::select('tickets.*')
                ->orwhere('tickets.title','like',"%{$data['search']}%")
                ->orWhere('tickets.description','like',"%{$data['search']}%")
                ->get(100);

        return view('support.tickets.index',$data);
    }


    //
    public function saveNumPerPage(Request $request)
    {
        $this->rules = array(
            'numPerPage' => ['nullable',Rule::in(\App\Common::NUMPERPAGE)]
        );

        $request->validate($this->rules);

        \Cookie::queue('numPerPage', $request->input('numPerPage'), 86400*30);

        return redirect()->route('support.tickets.index');
    }


    //
    public function saveShowOnly(Request $request)
	{
        $this->rules = array(
            'status' => ['nullable',Rule::in(\App\Ticket::STATUS)]
        );

        $request->validate($this->rules);

        \Cookie::queue('ticketsShowOnly', $request->input('status'), 86400*30);

        return redirect()->route('support.tickets.index');
	}
}
