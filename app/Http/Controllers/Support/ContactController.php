<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Auth;

class ContactController extends Controller
{
    //
    public function __construct()
    {

    }


    //
    public function index()
    {
        return view('support.contact.index');
    }


    //
    public function create(Request $request)
    {
        $this->_validate_recaptcha();

        $tmp = array(
            'full_name' => 'required|max:100'
            ,'phone' => 'required|max:100'
            ,'email' => 'required|email'
            ,'message' => 'required'
        );

        // form validation
        $request->validate($tmp);

        $this->_send_emails();
        $this->_record_contact_emails();

        return redirect()->route('support.contact.success');
    }


    //
    private function _send_emails()
    {
        $support = explode(',',config('app.support_email'));
        (empty($support)) ? $support[0] = "noreply@{$_SERVER['HTTP_HOST']}" : NULL;

        $subject = config('app.name') . ' Contact Form Submission';
        $message = $_POST['full_name']
            . "<br />"
            . $_POST['email']
            . "<br />"
            . $_POST['phone']
            . "<br />"
            . $_POST['message'];
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From:{$support[0]}";

        if(!empty($support) && config('app.env') != 'local')
            foreach($support as $k => $v)
                mail($v,$subject,$message,$headers);

        return;
    }


    //
    private function _record_contact_emails()
    {
        $input['to'] = config('app.support_email');
        $input['from'] = $_POST['email'];
        $input['name'] = $_POST['full_name'];
        $input['phone'] = $_POST['phone'];
        $input['message'] = nl2br($_POST['message']);

        $input['auth'] = (Auth::check()) ? serialize(Auth::user()) : 'This person was not logged in.';

        $server = $_SERVER;
        unset($server['DB_HOST']
            ,$server['DB_USERNAME']
            ,$server['DB_PASSWORD']
            ,$server['GOOGLE_RECAPTCHA_SECRET']);
        $input['server'] = serialize($server);
        $input['cookie'] = serialize($_COOKIE);
        $input['session'] = serialize(session()->all());
        $input['request'] = serialize($_REQUEST);

        return \App\Email::create($input);
    }


    //
    public function success()
    {
        return view('support.contact.success');
    }


    //
	private function _validate_recaptcha()
	{
		if($_SERVER['SERVER_PORT'] == 80)	// let local and dev bypass this, https required
			return TRUE;

		$url = "https://www.google.com/recaptcha/api/siteverify";
		$data = http_build_query([
			'secret' => config('app.google_recaptcha_secret')
			,'response' => $_POST['g-recaptcha-response']
			]);

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
        $response = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($response);
		return $response->success;
	}
}
