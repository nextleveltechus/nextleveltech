<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //
    public function __construct()
    {

    }


    //
    public function index()
    {
        return view('support.index.index');
    }
}
