<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function requireLoggedOut()
    {
        $this->middleware(function ($request, $next) {
            return (\Auth::check()) ? redirect()->route('index')->send() : $next($request);
        });
    }

    public function requireLoggedIn()
    {
        $this->middleware(function ($request, $next) {
            return (!\Auth::check()) ? redirect()->route('index')->send() : $next($request);
        });
    }

    public function requireUserType( $types=array() )
    {
        $this->types = $types;

        $this->middleware(function ($request, $next) {
            $user = \Auth::user();

            if(!in_array($user->type,$this->types))
                return redirect()->route('index')->send();

            return $next($request);
        });
    }

    //
    public function _create_export( $name = NULL, $payments = array() )
    {
        $extension = "csv";
        $filename = strtolower(substr(sha1(time()),0,20));

        $fp = fopen("{$filename}.{$extension}", 'w');

        $payments = $payments->toArray();

        // build header line
        foreach($payments[0] as $k => $v)
            $arr[] = $k;

        fputcsv($fp, $arr);

        foreach($payments as $k => $v)
            fputcsv($fp, $v);

        fclose($fp);

        $size = getimagesize("{$filename}.{$extension}");

        $files['basepath'] = config('app.file_basepath');
        $files['title'] = $name;
        $files['raw_name'] = $filename;
        $files['file_ext'] = $extension;
        $files['file_size'] = filesize("{$filename}.{$extension}");
        $files['image_width'] = @$size[0];
        $files['image_height'] = @$size[1];
        $files['image_size_str'] = @$size[3];
        $files['mime'] = @$size['mime'];
        $files['userId'] = \Auth::user()->id;
        $files['instanceType'] = \App\File::INSTANCE_TYPE['USER'];
        $files['instanceId'] = \Auth::user()->id;

        $file = \App\File::create($files);

        $directory = \App\Common::create_dir_from_id($files['basepath'],$file->id);
        $new_file = "{$directory}/{$files['raw_name']}.{$files['file_ext']}";

		// move file
		return rename("{$filename}.{$extension}",$new_file);
    }

    //
    public function _reflect_timezones( $obj = array() )
    {
        if(empty($obj))
            return;

        $arr = $obj->toArray();

        foreach($arr as $k => $v)
            foreach($v as $ke => $va)
                if(in_array($ke, ['created_at','updated_at']))
                    $obj[$k][$ke] = displayFromStringDateTimeRaw($va);

        return $obj;
    }


    //
    public function _upload_file( $type = NULL )
    {
        if(!empty($_FILES['userfile']['error']))
            return;

        $info = new \SplFileInfo($_FILES['userfile']['name']);
        $extension = strtolower($info->getExtension());
        $filename = strtolower(substr(sha1(time()),0,20));

        $size = getimagesize($_FILES['userfile']['tmp_name']);

        $files['basepath'] = config('app.file_basepath');
        $files['title'] = '';
        $files['raw_name'] = $filename;
        $files['file_ext'] = $extension;
        $files['file_size'] = $_FILES['userfile']['size'];
        $files['image_width'] = @$size[0];
        $files['image_height'] = @$size[1];
        $files['image_size_str'] = @$size[3];
        $files['mime'] = @$size['mime'];
        $files['userId'] = \Auth::user()->id;
        $files['instanceType'] = $type;
        $files['instanceId'] = \Auth::user()->id;

        $file = \App\File::create($files);

        $directory = \App\Common::create_dir_from_id($files['basepath'],$file->id);
        $new_file = "{$directory}/{$files['raw_name']}.{$files['file_ext']}";

		// move file
		return copy($_FILES['userfile']['tmp_name'],$new_file);
    }
}
