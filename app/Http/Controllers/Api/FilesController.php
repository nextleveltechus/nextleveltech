<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //
     public function license()
     {
        if(!\Auth::check()){
            print readfile('./public/images/noFile.png');
            exit;
        }

        $file = \App\File::where('instanceId',\Auth::user()->id)
            ->where('instanceType',\App\File::INSTANCE_TYPE['LICENSE'])
            ->whereNull('deleted_at')
            ->first();

        if(empty($file)){
            print readfile('./public/images/noFile.png');
            exit;
        }

        $directory = \App\Common::create_dir_from_id( $file->basepath, $file->id );
        $full_file = "{$directory}/{$file->raw_name}.{$file->file_ext}";

 		$files = array_values(array_filter(glob("{$full_file}*"),'is_file'));

 		if(!empty($files)){
 			header("Content-Type: {$file['mime']}");
 			print readfile($files[0]);
 			exit;
 		}

 		print readfile('./public/images/noFile.png');
 		exit;
     }


    //
    public function show($id)
    {
        $file = \App\File::find($id);

        if($file->userId != \Auth::user()->id && \Auth::user()->type != \App\User::TYPE['SYSTEM ADMINISTRATOR'])
            return;

        if(empty($file))
            return;

        $directory = \App\Common::create_dir_from_id( $file->basepath, $id );
        $full_file = "{$directory}/{$file->raw_name}.{$file->file_ext}";

		$files = array_values(array_filter(glob("{$full_file}*"),'is_file'));

		if(!empty($files)){
			header("Content-Type: {$file['mime']}");
			print readfile($files[0]);
			exit;
		}

		print readfile('./public/images/noFile.png');
		exit;
    }


    //
    public function download($id)
    {
        $file = \App\File::find($id);

        if($file->userId != \Auth::user()->id && \Auth::user()->type != \App\User::TYPE['SYSTEM ADMINISTRATOR'])
            return;

        if(empty($file))
            return;

        $directory = \App\Common::create_dir_from_id( $file->basepath, $id );
        $full_file = "{$directory}/{$file->raw_name}.{$file->file_ext}";

		$files = array_values(array_filter(glob("{$full_file}*"),'is_file'));

		if(!empty($files)){
            header("Content-Type: {$file['mime']}");
            header("Content-Disposition: attachment; filename={$file['raw_name']}.{$file['file_ext']}");
            header('Pragma: no-cache');
            readfile($files[0]);
			exit;
		}

        header ('Content-Type: image/png');
		print readfile('./images/noFile.png');
		exit;
    }
}
