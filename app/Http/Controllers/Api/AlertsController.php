<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     //
    public function toggleHelpBlocks(Request $request)
    {
        if(\Cookie::get('showHelpBlocks')){
            \Cookie::queue('showHelpBlocks', FALSE, 86400*30);
            print "gray";
        } else {
            \Cookie::queue('showHelpBlocks', TRUE, 86400*30);
            print "blue";
        }
    }
}
