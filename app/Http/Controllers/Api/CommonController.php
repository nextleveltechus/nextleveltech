<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Zipcode;
use Auth;

class CommonController extends Controller
{
     //
    public function returnCity(Request $request)
    {
        return @ZipCode::ZIPCODES[$request->zipcode]['city'];
    }


    //
    public function returnState(Request $request)
    {
        return @ZipCode::ZIPCODES[$request->zipcode]['state'];
    }


    //
    public function returnCounty(Request $request)
    {
        return @\App\Helpers\Counties::ZIPCODES[$request->zipcode]['name'];
    }
}
