<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Rules\CheckUploadFile;

class UploadsController extends Controller
{
    var $organizationId = NULL;

    var $rules = array();

    //
    public function __construct()
    {
        $this->requireLoggedIn();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('fileNumPerPage')??config('app.default_num_per_page');
        $data['showOnly'] = \Cookie::get('fileShowOnly');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $in = (empty($data['showOnly'])) ? \App\File::return_type_array() : array($data['showOnly']);

        $data['files'] = \App\File::orderBy($sort,$order)
            ->whereIn('files.file_ext',$in)
            ->whereIn('instanceType',['USER'])
            ->where('instanceId',\Auth::user()->id)
            ->whereNull('deleted_at')
            ->paginate($data['numPerPage']);

        return view('account.uploads.index',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fileTitle' => 'required|max:100'
            ,'userfile' => ['nullable','file',new CheckUploadFile]
        ]);

        $this->_save_file($request->all());

        session()->flash('notice','The file was saved.');
        return redirect("/account/uploads/index");
    }


    private function _save_file( $file = array() )
    {
        if(!empty($_FILES['userfile']['error']))
            return;

        $info = new \SplFileInfo($_FILES['userfile']['name']);
        $extension = strtolower($info->getExtension());
        $filename = strtolower(substr(sha1(time()),0,20));

        $size = getimagesize($_FILES['userfile']['tmp_name']);

        $files['basepath'] = config('app.file_basepath');
        $files['title'] = $file['fileTitle'];
        $files['raw_name'] = $filename;
        $files['file_ext'] = $extension;
        $files['file_size'] = $_FILES['userfile']['size'];
        $files['image_width'] = @$size[0];
        $files['image_height'] = @$size[1];
        $files['image_size_str'] = @$size[3];
        $files['mime'] = @$size['mime'];
        $files['userId'] = \Auth::user()->id;
        $files['instanceType'] = \App\File::INSTANCE_TYPE['USER'];
        $files['instanceId'] = \Auth::user()->id;

        $file = \App\File::create($files);

        $directory = \App\Common::create_dir_from_id($files['basepath'],$file->id);
        $new_file = "{$directory}/{$files['raw_name']}.{$files['file_ext']}";

		// move file
		copy($_FILES['userfile']['tmp_name'],$new_file);
    }


    //
    public function destroy(Request $request)
    {
        $file = \App\File::find((int)$request->post('fileId'));

        if(empty($file))
            return redirect("/account/uploads/index");

        \App\File::where('id',$file->id)
            ->update(['deleted_at' => date("Y-m-d H:i:s")]);

        session()->flash('notice','The file has been removed.');
        return redirect("/account/uploads/index");
    }


    //
    public function searchQuick(Request $request)
    {
        $data['search'] = $request->input('search');

        $data['files'] = collect();

        if(!empty($data['search']))
            $data['files'] = \App\File::where('instanceType','USER')
                ->where('instanceId',\Auth::user()->id)
                ->where('title','like',"%{$data['search']}%")
                ->whereNull('deleted_at')
                ->get();

        return view('account.uploads.index',$data);
    }


    //
    public function saveNumPerPage(Request $request)
    {
        $this->rules = array(
            'numPerPage' => ['nullable',Rule::in(\App\Common::NUMPERPAGE)]
        );

        $request->validate($this->rules);

        \Cookie::queue('fileNumPerPage', $request->input('numPerPage'), 86400*30);

        return redirect("/account/uploads/index");
    }


    //
    function saveShowOnly(Request $request)
	{
        $this->rules = array(
            'type' => ['nullable',Rule::in(\App\File::return_type_array())]
        );

        $request->validate($this->rules);

        \Cookie::queue('fileShowOnly', $request->input('type'), 86400*30);

        return redirect("/account/uploads/index");
	}
}
