<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Auth;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->requireLoggedIn();
    }


    //
    public function index(Request $request)
    {
        if(!empty($_POST)){
            $tmp = array(
                'username' => ['required','alpha_num','between:6,25',Rule::unique('users')->ignore(Auth::user()->id)]
                ,'email' => 'nullable|email'
                ,'fname' => 'required|max:100'
                ,'lname' => 'required|max:100'
            );

            if(!empty($request->input('password'))){
                $tmp['password'] = 'required|between:6,25|regex:@[A-Z]@|regex:@[a-z]@|regex:@[0-9]@|regex:@[\W]@';
                $input = request()->except(['_token']);
                $input['password'] = (!empty($request->input('password'))) ? password_hash($input['password'],PASSWORD_BCRYPT) : NULL;
            } else {
                $input = request()->except(['_token','password']);
            }

            // form validation
            $request->validate($tmp);

            // save the user
            \App\User::where('id',Auth::user()->id)->update($input);

            session()->flash('notice','Your account has been updated.');

            return redirect()->route('account');
        }

        $data['user'] = \App\User::find(Auth::user()->id);

        return view('account.index.form',$data);
    }
}
