<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{
    //
    public function __construct()
    {

    }


    //
    public function index()
    {
        return view('info.index');
    }


    //
    public function terms()
    {
        return view('info.terms');
    }


    //
    public function privacy()
    {
        return view('info.privacy');
    }
}
