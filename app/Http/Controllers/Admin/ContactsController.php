<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    var $contactId = NULL;

    var $rules = [
        'fname' => 'required|max:100'
        ,'lname' => 'required|max:100'
        ,'email' => 'nullable|email'
        ,'phone' => 'nullable'
        ,'street1' => 'nullable'
        ,'street2' => 'nullable'
        ,'city' => 'nullable'
        ,'state' => 'nullable'
        ,'zipcode' => 'nullable'
        ,'notes' => 'nullable'
    ];

    public function __construct(Request $request, $slashdata = NULL)
    {
        $this->requireLoggedIn();
        $this->requireUserType(array('SYSTEM ADMINISTRATOR'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('contactsNumPerPage')??config('app.default_num_per_page');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $data['contacts'] = \App\Contact::orderBy($sort,$order)
            ->paginate($data['numPerPage']);

        return view('admin.contacts.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['url'] = route('admin.contacts.store');

        $data['states'] = \App\Helpers\State::return_for_dropdown();

        return view('admin.contacts.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate( $this->rules );

        \App\Contact::create($request->all());

        session()->flash('notice','The contact has been created.');
        return redirect("/admin/contacts/index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['contact'] = \App\Contact::find($id);

        if(empty($data['contact']))
            return redirect()->route('admin.index.index');

        $data['url'] = route('admin.contacts.update',$id);
        $data['method'] = 'put';

        $data['states'] = \App\Helpers\State::return_for_dropdown();

        return view('admin.contacts.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate( $this->rules );

        \App\Contact::where('id',$id)
            ->update($request->except(['_token','_method']));

        session()->flash('notice','The contact has been updated.');
        return redirect("/admin/contacts/index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        \App\Contact::where('id',$id)
            ->delete();

        session()->flash('notice','The contact has been deleted.');
        return redirect("/admin/contacts/index");
    }

    //
    public function searchQuick(Request $request)
    {
        $data['search'] = $request->input('search');

        $data['contacts'] = collect();

        if(!empty($data['search']))
            $data['contacts'] = \App\Contact::where('fname','like',"%{$data['search']}%")
                ->orWhere('lname','like',"%{$data['search']}%")
                ->orWhere('email','like',"%{$data['search']}%")
                ->orWhere('notes','like',"%{$data['search']}%")
                ->get();

        return view('admin.contacts.index',$data);
    }


    //
    public function saveNumPerPage(Request $request)
    {
        $this->rules = array(
            'numPerPage' => ['nullable',Rule::in(\App\Common::NUMPERPAGE)]
        );

        $request->validate($this->rules);

        \Cookie::queue('contactsNumPerPage', $request->input('numPerPage'), 86400*30);

        return redirect("/admin/contacts/index");
    }
}
