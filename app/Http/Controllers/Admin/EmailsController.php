<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailsController extends Controller
{
    //
    public function __construct()
    {
        $this->requireLoggedIn();
        $this->requireUserType(array('SYSTEM ADMINISTRATOR'));
    }


    //
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('numPerPage')??config('app.default_num_per_page');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $data['emails'] = \App\Email::orderBy($sort,$order)
            ->where('deleted','FALSE')
            ->paginate($data['numPerPage']);

        return view('admin.emails.index',$data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // soft delete
    public function destroy(Request $request)
    {
        //
        $id = (int)$request->input('id');

        $email = \App\Email::where('id',$id)
            ->get();

        if($email->isEmpty())
            return redirect()->route('admin.emails');

        \App\Email::where('id',$id)
            ->update(['deleted' => 'TRUE']);

        session()->flash('notice','The email has been deleted.');
        return redirect()->route('admin.emails');
    }


    //
    public function show($id)
    {
        $data['email'] = \App\Email::find($id);

        return view('admin.emails.show',$data);
    }


    //
    public function searchQuick(Request $request)
    {
        $data['start'] = 0;
        $data['keyword'] = $request->input('keyword');
        $data['emails'] = array();

        if(!empty($data['keyword'])){
            $data['emails'] = \App\Email::where('from','like',"%{$data['keyword']}%")
                ->orWhere('name','like',"%{$request->input('keyword')}%")
                ->orWhere('message','like',"%{$request->input('keyword')}%")
                ->get();
        }

        return view('admin.emails.index',$data);
    }


    //
    public function saveNumPerPage()
    {
        \Cookie::queue('numPerPage', $_POST['numPerPage'], 86400*30);
        $pieces = explode('?',$_SERVER['HTTP_REFERER']);
        return redirect($pieces[0]);
    }
}
