<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Rules\CheckUploadFile;
use \Auth;

class TicketsController extends Controller
{
    var $rules = array();
    var $req = array();

    //
    public function __construct()
    {
        $this->requireLoggedIn();
        $this->requireUserType(array('SYSTEM ADMINISTRATOR'));

        $this->rules = array(
            'title' => 'required|max:100'
            ,'description' => 'required'
            ,'type' => ['required',Rule::in(array_keys(\App\Ticket::TYPE))]
            ,'status' => ['required',Rule::in(array_keys(\App\Ticket::STATUS))]
            ,'priority' => ['required',Rule::in(array_keys(\App\Ticket::PRIORITY))]
            ,'estimatedTime' => 'nullable|integer'
            ,'percentageComplete' => ['nullable','integer',Rule::in(array_keys(\App\Common::PERCENTS))]
            ,'assigneeId' => 'required|integer'
            ,'userfile' => ['nullable','file',new CheckUploadFile]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('numPerPage')??config('app.default_num_per_page');
        $data['ticketsShowOnly'] = \Cookie::get('ticketsShowOnly');
        $data['ticketsShowOnlyAdmin'] = \Cookie::get('ticketsShowOnlyAdmin');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $in = (empty($data['ticketsShowOnly'])) ? array("NEW" => "New","DESIGN" => "Design","IN PROGRESS" => "In Progress","FEEDBACK" => "Feedback","QUALITY ASSURANCE" => "Quality Assurance","QUALITY ASSURANCE PASSED" => "Quality Assurance Passed","RELEASED" => "Released","CLEANUP" => "Cleanup") : array($data['ticketsShowOnly']);

        $inAdmin = array($data['ticketsShowOnlyAdmin']);
        if(empty($data['ticketsShowOnlyAdmin']))
            $inAdmin = ['id' => Auth::user()->id];
        elseif($data['ticketsShowOnlyAdmin'] == 'all')
            $inAdmin = \App\User::system_admins_array();

        $data['tickets'] = \App\Ticket::orderBy($sort,$order)
            ->select(['tickets.*','u.fname','u.lname','a.fname as assigneefname','a.lname as assigneelname'])
            ->whereIn('tickets.status',$in)
            ->whereIn('tickets.assigneeId',$inAdmin)
            ->join('users as u','tickets.userId','u.id')
            ->join('users as a','tickets.assigneeId','a.id')
            ->paginate($data['numPerPage']);

        return view('admin.tickets.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($_FILES['userfile']['error']))
            $this->rules['fileTitle'] = 'required|max:100';

        // form validation
        $request->validate($this->rules);

        $input = $request->all();
        ($input['startDate']) ? $input['startDate'] = strtotime($input['startDate']) : NULL;
        ($input['endDate']) ? $input['endDate'] = strtotime($input['endDate']) : NULL;
        $input['userId'] = Auth::user()->id;

        $ticket = \App\Ticket::create($input);

        $this->_save_file($ticket, $request->input('fileTitle'));

        session()->flash('notice','The ticket has been created.');
        return redirect()->route('admin.tickets.index');
    }


    private function _save_file( $ticket = array(), $fileTitle = null )
    {
        if(!empty($_FILES['userfile']['error']))
            return;

        $info = new \SplFileInfo($_FILES['userfile']['name']);
        $extension = strtolower($info->getExtension());
        $filename = strtolower(substr(sha1(time()),0,20));

        $size = getimagesize($_FILES['userfile']['tmp_name']);

        $files['basepath'] = config('app.file_basepath');
        $files['title'] = $fileTitle;
        $files['raw_name'] = $filename;
        $files['file_ext'] = $extension;
        $files['file_size'] = $_FILES['userfile']['size'];
        $files['image_width'] = @$size[0];
        $files['image_height'] = @$size[1];
        $files['image_size_str'] = @$size[3];
        $files['mime'] = @$size['mime'];
        $files['userId'] = Auth::user()->id;
        $files['instanceType'] = \App\File::INSTANCE_TYPE['TICKET'];
        $files['instanceId'] = $ticket['id'];

        $file = \App\File::create($files);

        $directory = \App\Common::create_dir_from_id($files['basepath'],$file->id);
        $new_file = "{$directory}/{$files['raw_name']}.{$files['file_ext']}";

		// move file
		copy($_FILES['userfile']['tmp_name'],$new_file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ticket'] = \App\Ticket::where('tickets.id',$id)
            ->select(['tickets.*','u.fname','u.lname','a.fname as assigneefname','a.lname as assigneelname'])
            ->join('users as u','tickets.userId','u.id')
            ->join('users as a','tickets.assigneeId','a.id')
            ->first();

        $data['ticketResponses'] = \App\TicketResponse::orderBy('ticket_responses.created_at','DESC')
            ->select('users.*','ticket_responses.*')
            ->where('ticketId',$id)
            ->join('users','userId','users.id')
            ->get();

        $data['files'] = \App\File::orderBy('created_at','DESC')
            ->where('instanceId',$id)
            ->where('instanceType',\App\File::INSTANCE_TYPE['TICKET'])
            ->get();

        return view('admin.tickets.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(empty($_FILES['userfile']['error']))
            $this->rules['fileTitle'] = 'required|max:100';

        // form validation
        $request->validate($this->rules);

        $input = $request->except(['_method','_token','fileTitle','userId']);
        ($input['startDate']) ? $input['startDate'] = strtotime($input['startDate']) : NULL;
        ($input['endDate']) ? $input['endDate'] = strtotime($input['endDate']) : NULL;

        $ticket = \App\Ticket::find($id);

        \App\Ticket::find($id)
            ->update($input);

        \App\Ticket::find($id)
            ->touch();

        $updates = array();
        $skipFields = array('description','response','fileTitle','submit','userfile');
		foreach($input as $k => $v){
			if(!in_array($k,$skipFields) && $_POST[$k] != $ticket[$k]){
				$first = ($ticket[$k]) ? ucwords(strtolower($ticket[$k])) : 'Null';
				$second = ($_POST[$k]) ? ucwords(strtolower($_POST[$k])) : 'Null';
                ($k == 'startDate') ? $first = displayFormattedDate($ticket[$k]) : NULL;
                ($k == 'endDate') ? $first = displayFormattedDate($ticket[$k]) : NULL;

                if($first != $second)
		            array_push($updates,titleize($k) . " changed from {$first} to {$second}.");
			}

			if($k == 'description' && $input[$k] != $ticket[$k])
				array_push($updates,"The description has been changed.");
		}

        if(empty($_FILES['userfile']['error']))
            array_push($updates,"The file {$request->input('fileTitle')} was uploaded.");

        $input = $request->except(['_method','_token']);
		$input['updates'] = serialize($updates);
        $input['userId'] = Auth::user()->id;
        $input['ticketId'] = $id;
        \App\TicketResponse::create($input);

        $this->_save_file(['id' => $id, 'title' => $request->input('fileTitle')]);

        session()->flash('notice','The ticket has been updated.');
        return redirect()->route('admin.tickets.edit',$id);
    }


    //
    public function searchQuick(Request $request)
    {
        $data['search'] = $request->input('search');

        $data['tickets'] = collect();

        if(!empty($data['search']))
            $data['tickets'] = \App\Ticket::select(['tickets.*','u.fname','u.lname','a.fname as assigneefname','a.lname as assigneelname'])
                ->where('tickets.title','like',"%{$data['search']}%")
                ->orWhere('tickets.description','like',"%{$data['search']}%")
                ->join('users as u','tickets.userId','u.id')
                ->join('users as a','tickets.assigneeId','a.id')
                ->get();

        return view('admin.tickets.index',$data);
    }


    //
    public function saveNumPerPage(Request $request)
    {
        $this->rules = array(
            'numPerPage' => ['nullable',Rule::in(\App\Common::NUMPERPAGE)]
        );

        $request->validate($this->rules);

        \Cookie::queue('numPerPage', $request->input('numPerPage'), 86400*30);

        return redirect()->route('admin.tickets.index');
    }


    //
    function saveShowOnly(Request $request)
	{
        $this->rules = array(
            'status' => ['nullable',Rule::in(\App\Ticket::STATUS)]
        );

        $request->validate($this->rules);

        \Cookie::queue('ticketsShowOnly', $request->input('status'), 86400*30);

        return redirect()->route('admin.tickets.index');
	}


    //
    function saveShowOnlyAdmin(Request $request)
	{
        \Cookie::queue('ticketsShowOnlyAdmin', $request->input('assigneeId'), 86400*30);

        return redirect()->route('admin.tickets.index');
	}
}
