<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Auth;

class UsersController extends Controller
{
    //
    public function __construct()
    {
        $this->requireLoggedIn();
        $this->requireUserType(array('SYSTEM ADMINISTRATOR'));
    }


    //
    public function index(Request $request, $slashdata = NULL)
    {
        $sort = get_param('sort',$slashdata)??'id';
        $order = get_param('order',$slashdata)??'desc';

        $data['numPerPage'] = \Cookie::get('numPerPage')??config('app.default_num_per_page');
        $data['adminUsersShowOnly'] = \Cookie::get('adminUsersShowOnly');
        $data['start'] = (isset($_GET['page'])) ? $data['numPerPage'] * $_GET['page'] - $data['numPerPage'] : 0;

        $in = (empty($data['adminUsersShowOnly'])) ? \App\User::TYPE : array($data['adminUsersShowOnly']);

        $data['users'] = \App\User::orderBy($sort,$order)
            ->whereIn('type',$in)
            ->paginate($data['numPerPage']);

        return view('admin.users.index',$data);
    }


    //
    public function searchQuick(Request $request)
    {
        $data['start'] = 0;
        $data['keyword'] = $request->input('keyword');
        $data['users'] = array();

        if(!empty($data['keyword'])){
            $data['users'] = \App\User::where('username','like',"%{$data['keyword']}%")
                ->orWhere('email','like',"%{$request->input('keyword')}%")
                ->orWhere('fname','like',"%{$request->input('keyword')}%")
                ->orWhere('lname','like',"%{$request->input('keyword')}%")
                ->get();
        }

        return view('admin.users.index',$data);
    }


    //
    public function saveNumPerPage()
    {
        \Cookie::queue('numPerPage', $_POST['numPerPage'], 86400*30);
        $pieces = explode('?',$_SERVER['HTTP_REFERER']);
        return redirect($pieces[0]);
    }


    //
    public function showOnly()
    {
        \Cookie::queue('adminUsersShowOnly', $_POST['adminUsersShowOnly'], 86400*30);
        $pieces = explode('?',$_SERVER['HTTP_REFERER']);
        return redirect($pieces[0]);
    }


    //
    public function add(Request $request)
    {
        if(!empty($_POST)){
            $tmp = array(
                'username' => 'required|alpha_num|between:6,25|unique:users,username'
                ,'password' => 'required|between:6,25|regex:@[A-Z]@|regex:@[a-z]@|regex:@[0-9]@|regex:@[\W]@'
                ,'email' => 'nullable|email'
                ,'fname' => 'required|max:100'
                ,'lname' => 'required|max:100'
            );

            // form validation
            $request->validate($tmp);

            $this->_email_new_user($request->all());

            // save the user
            $input = $request->all();
            $input['password'] = password_hash($input['password'],PASSWORD_BCRYPT);
            \App\User::create($input);

            session()->flash('notice','The user has been created.');

            return redirect()->route('admin.users');
        }

        return view('admin.users.form');
    }


    //
    private function _email_new_user($input)
    {
        if(empty($input['notify']))
            return;

        $support = explode(',',config('app.support_email'));
        (empty($support)) ? $support[0] = "noreply@{$_SERVER['HTTP_HOST']}" : NULL;

        $subject = "Login Information for your ".config('app.name')." account";
        $message = view('admin.users._email_new_user',$input);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From:{$support[0]}";

        if(config('app.env') == 'production')
            mail($input['email'],$subject,$message,$headers);

        return $this->_record_emails($input, 'info@'.$_SERVER['HTTP_HOST'], $subject, $message);
    }


    //
    public function edit(Request $request, $slashdata = NULL)
    {
        $data['user'] = \App\User::find((int)get_param('id',$slashdata));

        if(empty($data['user']))
            return redirect()->route('admin.users');

        if(!empty($_POST)){
            $tmp = array(
                'username' => ['required','alpha_num','between:6,25',Rule::unique('users')->ignore($data['user']->id)]
                ,'type' => ['required',Rule::in(array_keys(\App\User::TYPE))]
                ,'status' => ['required',Rule::in(array_keys(\App\User::STATUS))]
                ,'email' => 'nullable|email'
                ,'fname' => 'required|max:100'
                ,'lname' => 'required|max:100'
            );

            if(!empty($request->input('password'))){
                $tmp['password'] = 'required|between:6,25|regex:@[A-Z]@|regex:@[a-z]@|regex:@[0-9]@|regex:@[\W]@';
                $input = request()->except(['_token']);
                $input['password'] = (!empty($request->input('password'))) ? password_hash($input['password'],PASSWORD_BCRYPT) : NULL;
            } else {
                $input = request()->except(['_token','password']);
            }

            // form validation
            $request->validate($tmp);

            $input['type'] = strtoupper(\App\User::TYPE[$request->post('type')]);
            $input['status'] = strtoupper(\App\User::STATUS[$request->post('status')]);

            // save the user
            \App\User::where('id',$data['user']->id)->update($input);

            session()->flash('notice','The user has been updated.');

            return redirect()->route('admin.users');
        }

        return view('admin.users.form',$data);
    }
}
