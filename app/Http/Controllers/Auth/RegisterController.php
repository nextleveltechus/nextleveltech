<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_num|between:6,25|unique:users,username'
            ,'password' => 'required|between:6,25|regex:@[A-Z]@|regex:@[a-z]@|regex:@[0-9]@|regex:@[\W]@'
            ,'confirm' => 'required|same:password'
            ,'email' => 'required|email'
            ,'timezone' => ['required',Rule::in(\App\User::TIMEZONE)]
            ,'fname' => 'required|max:100'
            ,'lname' => 'required|max:100'
            ,'terms_and_conditions' => 'accepted'
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->_validate_recaptcha();

        $data['password'] = bcrypt($data['password']);
        $data['type'] = strtoupper(\App\User::TYPE['USER']);
        $data['status'] = strtoupper(\App\User::STATUS['ACTIVE']);

        $user = \App\User::create($data);

        //login
        Auth::login($user);

        $name = Auth::user()->fname . ' ' . Auth::user()->lname;
        session()->flash('notice',"You are now logged in as {$name}.");

        return $user;
    }


    //
	private function _validate_recaptcha()
	{
		if($_SERVER['SERVER_PORT'] == 80)	// let local and dev bypass this, https required
			return TRUE;

		$url = "https://www.google.com/recaptcha/api/siteverify";
		$data = http_build_query([
			'secret' => config('app.google_recaptcha_secret')
			,'response' => $_POST['g-recaptcha-response']
			]);

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
        $response = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($response);
		return $response->success;
	}
}
