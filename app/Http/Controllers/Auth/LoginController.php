<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Helpers\Common;

use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }


    public function username()
    {
        return 'username';
    }


    //
    public function showLoginForm()
    {
        $data['states'] = \App\Helpers\State::return_for_dropdown();

        return view('auth.login',$data);
    }


    //
    public function auto()
    {
        $data = unserialize($_POST['user']);

        $user = \App\User::where('id',(int)$data['id'])
            ->first();

        if(empty($user))
            return redirect()->route('index');

        if($user->username != $data['username'] || $user->password != $data['password'])
            return redirect()->route('index');

        Auth::login($user);

        $name = Auth::user()->fname . ' ' . Auth::user()->lname;
        session()->flash('notice',"You are now logged in as {$name}.");
        return redirect()->route('index');
    }


    //
    public function token_login(Request $request, $slashdata)
    {
        $username = get_param('username',$slashdata);
        $token = get_param('token',$slashdata);

        $user = \App\User::where('username',$username)
            ->where('password_token',$token)
            ->where('status',strtoupper(\App\User::STATUS['ACTIVE']))
            ->where('password_token_timeout','>=',time())
            ->first();

        if(empty($user)){
            session()->flash('error',"We're sorry, we could not log you in at this time.");
            return redirect()->route('index');
        }

        Auth::login($user);

        \App\User::where('username',$username)
            ->update(['password_token' => '', 'password_token_timeout' => 0]);

        $name = Auth::user()->fname . ' ' . Auth::user()->lname;
        session()->flash('notice',"You are now logged in as {$name}.  Pleasse change your password now.");
        return redirect()->route('account');
    }


    //
    public function forgot_username_success(Request $request)
    {
        $rules = array(
            'email' => 'required|email'
        );

        $request->validate($rules);

        $users = \App\User::where('email',$request->input('email'))
                    ->get();

        if(!$users->isEmpty())
            $this->_send_forgot_username_email($request->input('email'), $users);

        return view('auth.login.forgot_username_success');
    }


    private function _send_forgot_username_email($email, $users)
    {
        if($users->isEmpty())
            return;

        $data['users'] = $users;
        $support = explode(',',config('app.support_email'));
        (empty($support)) ? $support[0] = "noreply@{$_SERVER['HTTP_HOST']}" : NULL;

        $subject = config('app.name') . ' Username Recovery';
        $message = view('auth.login._send_forgot_username_email',$data);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From:{$support[0]}";

        if(config('app.env') != 'local')
            mail($email,$subject,$message,$headers);

        return;
    }


    //
    public function lost_password_success(Request $request)
    {
        $rules = array(
            'username' => 'required|alpha_num'
        );

        $request->validate($rules);

        $user = \App\User::where('username',$request->input('username'))
                    ->first();

        if(!empty($user)){
            $token = sha1(time().$user->username);

            \App\User::where('username',$request->input('username'))
                ->where('status',strtoupper(\App\User::STATUS['ACTIVE']))
                ->update(['password_token_timeout' => time()+900, 'password_token' => $token]);

            $user = \App\User::where('username',$request->input('username'))
                ->where('status',strtoupper(\App\User::STATUS['ACTIVE']))
                ->first();

            $this->_send_lost_password_email($user);
        }

        return view('auth.login.lost_password_success');
    }


    private function _send_lost_password_email($user)
    {
        if(!empty($user)){
            $support = explode(',',config('app.support_email'));
            (empty($support)) ? $support[0] = "noreply@{$_SERVER['HTTP_HOST']}" : NULL;

            $subject = config('app.name') . ' Password Recovery';
            $message = view('auth.login._send_lost_password_email',['user' => $user]);
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "From:{$support[0]}";

            if(config('app.env') != 'local')
                mail($user->email,$subject,$message,$headers);
        }

        return view('auth.login.forgot_username_success');
    }
}
