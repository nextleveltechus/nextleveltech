const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js('resources/js/main.js', 'public/js')
     .js('node_modules/bootstrap-sass/assets/javascripts/bootstrap.js', 'public/js')
     .scripts(['public/js/main.js',
         'public/js/bootstrap.js'],
         'public/js/all.js');

 mix.sass('resources/sass/imports.scss','public/css')
     .sass('node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss','public/css')
     .sass('resources/sass/style.scss','public/css')
     .styles(['public/css/imports.css','public/css/_bootstrap.css','public/css/style.css'],
         'public/css/all.css');
